package oc.adventure.commande.ms.entity.contract;

import oc.adventure.shared.components.entity.contract.AdventureParent;

import java.util.Set;

public interface Adventure extends AdventureParent {
    Set<Session> getSessions();
}
