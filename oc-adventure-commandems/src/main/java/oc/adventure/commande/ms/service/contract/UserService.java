package oc.adventure.commande.ms.service.contract;

import oc.adventure.shared.components.dto.commande.user.UserInDto;
import oc.adventure.shared.components.dto.commande.user.UserOutDto;


public interface UserService {
    UserOutDto findByExternalId(String externalId);

    UserInDto create(UserInDto userInDto);

    UserInDto update(UserInDto userInDto);

    boolean delete(String externalId);
}
