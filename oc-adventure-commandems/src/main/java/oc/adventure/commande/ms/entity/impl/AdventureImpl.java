package oc.adventure.commande.ms.entity.impl;

import oc.adventure.commande.ms.entity.contract.Adventure;
import oc.adventure.commande.ms.entity.contract.Session;
import oc.adventure.shared.components.entity.AdventureParentImpl;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "adventure")
public class AdventureImpl extends AdventureParentImpl implements Adventure {

    @OneToMany(mappedBy = "adventure", fetch = FetchType.LAZY, targetEntity = SessionImpl.class)
    Set<Session> sessions = new HashSet<>();

    public AdventureImpl() {
    }

    @Override
    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }
}
