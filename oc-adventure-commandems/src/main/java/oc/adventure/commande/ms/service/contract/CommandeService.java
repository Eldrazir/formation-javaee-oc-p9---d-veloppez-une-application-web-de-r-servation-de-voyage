package oc.adventure.commande.ms.service.contract;

import com.stripe.exception.*;
import oc.adventure.shared.components.dto.commande.commande.CommandeInDto;
import oc.adventure.shared.components.dto.commande.commande.CommandeOutDto;

import java.util.List;

public interface CommandeService {
    void register(CommandeInDto commandeInDto) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException;

    List<CommandeOutDto> getAllByUser(String userExtId);
}
