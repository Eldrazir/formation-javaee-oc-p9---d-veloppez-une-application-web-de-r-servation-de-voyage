package oc.adventure.commande.ms.entity.contract;

public interface CommandeSession {
    Long getId();

    String getExternalId();

    int getQuantity();

    Commande getCommande();

    Session getSession();
}
