package oc.adventure.commande.ms.repository.contract;

import oc.adventure.commande.ms.entity.contract.Commande;

public interface CommandeRepository {
    Commande find(int id);

    Commande findByExternalId(String externalId);

    Commande save(Commande commande);

    boolean delete(Commande commande);
}
