package oc.adventure.commande.ms.entity.contract;

import oc.adventure.shared.components.entity.contract.UserParent;

import java.util.Set;

public interface User extends UserParent {

    Set<Commande> getCommandes();

    void addCommande(Commande commande);
}
