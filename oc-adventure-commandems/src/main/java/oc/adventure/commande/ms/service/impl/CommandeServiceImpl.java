package oc.adventure.commande.ms.service.impl;

import com.googlecode.jmapper.JMapper;
import com.stripe.exception.*;
import oc.adventure.commande.ms.entity.contract.Commande;
import oc.adventure.commande.ms.entity.contract.CommandeSession;
import oc.adventure.commande.ms.entity.contract.Session;
import oc.adventure.commande.ms.entity.contract.User;
import oc.adventure.commande.ms.entity.impl.CommandeImpl;
import oc.adventure.commande.ms.entity.impl.CommandeSessionImpl;
import oc.adventure.commande.ms.repository.contract.CommandeRepository;
import oc.adventure.commande.ms.repository.contract.CommandeSessionRepository;
import oc.adventure.commande.ms.repository.contract.SessionRepository;
import oc.adventure.commande.ms.repository.contract.UserRepository;
import oc.adventure.commande.ms.service.contract.CommandeService;
import oc.adventure.shared.components.dto.commande.commande.CommandeInDto;
import oc.adventure.shared.components.dto.commande.commande.CommandeOutDto;
import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionInDto;
import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionOutDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class CommandeServiceImpl implements CommandeService {

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private UserRepository<User> userRepository;


    @Autowired
    private JMapper<CommandeOutDto, CommandeImpl> commandeToOutDtoMapper;

    @Autowired
    private JMapper<CommandeImpl, CommandeInDto> inDtoToCommandeMapper;

    @Autowired
    private JMapper<CommandeSessionOutDto, CommandeSessionImpl> commandeSessionToOutDtoMapper;

    @Autowired
    private JMapper<CommandeSessionImpl, CommandeSessionInDto> inDtoToCommandeSessionMapper;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private CommandeSessionRepository commandeSessionRepository;

    @Autowired
    private StripeService stripeService;

    @Override
    public void register(CommandeInDto commandeInDto) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        this.stripeService.charge(commandeInDto.getTotalCost(), commandeInDto.getCurrency(), commandeInDto.getStripeEmail() +" " +commandeInDto.getUserExternalId(), commandeInDto.getStripeToken());
        Commande commande = inDtoToCommandeMapper.getDestination(commandeInDto);
        User user = this.userRepository.findByExtId(commandeInDto.getUserExternalId());
        user.addCommande(commande);
        ((CommandeImpl) commande).setPaiementDate(LocalDateTime.now());
        commande = this.commandeRepository.save(commande);
        Commande finalCommande = commande;
        commandeInDto.getCommandeSessionInDtoList().forEach(x ->{
            CommandeSession commandeSession = inDtoToCommandeSessionMapper.getDestination(x);
            Session session = this.sessionRepository.findByExternalId(x.getSessionExternalId());
            session.addCommandeSession(commandeSession);
            this.commandeSessionRepository.save(commandeSession);
            finalCommande.addCommandeSession(commandeSession);
        });

    }

    @Override
    public List<CommandeOutDto> getAllByUser(String userExtId){
        return this.userRepository.findByExtId(userExtId).getCommandes().stream().map(x ->  this.commandeToOutDtoMapper.getDestination((CommandeImpl) x)).collect(Collectors.toList());
    }

}
