package oc.adventure.commande.ms.repository.impl;

import oc.adventure.commande.ms.entity.contract.Adventure;
import oc.adventure.commande.ms.entity.impl.AdventureImpl;
import oc.adventure.commande.ms.repository.contract.AdventureRepository;
import oc.adventure.shared.components.repository.AdventureRepositoryParentImpl;
import oc.adventure.utils.JPAUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AdventureRepositoryImpl extends AdventureRepositoryParentImpl<Adventure> implements AdventureRepository<Adventure> {

    @Override
    public Adventure findByNameAdventure(String nameAdventure) {
        TypedQuery<Adventure> AdventureTypedQuery = entityManager
                .createNamedQuery(AdventureImpl.QN.FIND_BY_NAME_ADVENTURE, Adventure.class)
                .setParameter("name_adventure", nameAdventure);

        return JPAUtils.getSingleResult(AdventureTypedQuery);

    }

    @Override
    public Adventure findByExtId(String extId) {
        TypedQuery<Adventure> AdventureTypedQuery = entityManager
                .createNamedQuery(AdventureImpl.QN.FIND_BY_EXT_ID, Adventure.class).setParameter("ext_id", extId);
        return JPAUtils.getSingleResult(AdventureTypedQuery);
    }

    @Override
    public Adventure save(Adventure adventure) {
        Adventure AdventureFound = this.findByNameAdventure(adventure.getNameAdventure());
        if (AdventureFound != null)
            return null;

        String externalId = JPAUtils.GetBase62(8);
        Adventure AdventureFoundByExternal = this.findByExtId(externalId);
        while (AdventureFoundByExternal != null) {
            externalId = JPAUtils.GetBase62(8);
            AdventureFoundByExternal = this.findByExtId(externalId);
        }
        ((AdventureImpl) adventure).setExternalId(externalId);
        this.entityManager.persist(adventure);
        return adventure;
    }

    @Override
    public void delete(Adventure adventure) {
        if (!this.entityManager.contains(adventure))
            this.entityManager.merge(adventure);

        this.entityManager.remove(adventure);

    }

    @Override
    public Adventure merge(Adventure adventure, String nameAdventure) {
        Adventure AdventureFound = this.findByNameAdventure(nameAdventure);
        if (AdventureFound != null && !adventure.equals(AdventureFound))
            return null;

        if (!this.entityManager.contains(adventure))
            this.entityManager.merge(adventure);

        ((AdventureImpl) adventure).setNameAdventure(nameAdventure);

        return adventure;
    }

    @Override
    public Class<Adventure> getEntityTClass() {
        return Adventure.class;
    }
}
