package oc.adventure.commande.ms.controller;

import oc.adventure.commande.ms.service.contract.UserService;
import oc.adventure.shared.components.dto.commande.user.UserInDto;
import oc.adventure.shared.components.dto.commande.user.UserOutDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/ms/commande/user")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value    = "/{externalId}",
            produces = "application/json")
    @ResponseBody
    public UserOutDto getByExternalId(@PathVariable("externalId") String externalId){
        return this.userService.findByExternalId(externalId);
    }

    @PostMapping(value    = "/register",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserInDto  register(@RequestBody UserInDto userRegisterDto){
        return this.userService.create(userRegisterDto);
    }

    @PutMapping(value    = "/update",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserInDto update(@RequestBody UserInDto userInDto){
        return this.userService.update(userInDto);
    }
}
