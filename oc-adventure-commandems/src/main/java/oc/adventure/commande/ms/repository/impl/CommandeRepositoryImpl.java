package oc.adventure.commande.ms.repository.impl;

import oc.adventure.commande.ms.entity.contract.Commande;
import oc.adventure.commande.ms.entity.impl.CommandeImpl;
import oc.adventure.commande.ms.repository.contract.CommandeRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CommandeRepositoryImpl implements CommandeRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Commande find(int id){
        return this.entityManager.find( Commande.class, id);
    }

    @Override
    public Commande findByExternalId(String externalId){
        return null;
    }


    @Override
    public Commande save(Commande commande){
        ((CommandeImpl)commande).setExternalId(JPAUtils.GetBase62(8));
        ((CommandeImpl)commande).setPaiementDate(LocalDateTime.now());
        this.entityManager.persist(commande);
        return commande;
    }

    @Override
    public boolean delete(Commande commande){
        if(!this.entityManager.contains(commande))
            this.entityManager.persist(commande);

        this.entityManager.remove(commande);
        return this.findByExternalId(commande.getExternalId()) == null;
    }

}
