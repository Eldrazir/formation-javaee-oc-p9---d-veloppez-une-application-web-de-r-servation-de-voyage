package oc.adventure.commande.ms.controller;

import com.stripe.exception.*;
import oc.adventure.commande.ms.service.contract.CommandeService;
import oc.adventure.shared.components.dto.commande.commande.CommandeInDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/ms/commande")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CommandeController {

    @Autowired
    private CommandeService commandeService;

    @PostMapping(value = "/create",
                    consumes ="application/json")
    public void create(@RequestBody CommandeInDto commandeInDto){
        try {
            this.commandeService.register(commandeInDto);
        } catch (CardException | APIException | AuthenticationException | InvalidRequestException | APIConnectionException e) {
            e.printStackTrace();
        }
    }

}
