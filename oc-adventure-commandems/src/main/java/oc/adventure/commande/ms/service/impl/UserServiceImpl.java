package oc.adventure.commande.ms.service.impl;

import com.googlecode.jmapper.JMapper;
import oc.adventure.commande.ms.entity.contract.Commande;
import oc.adventure.commande.ms.entity.contract.CommandeSession;
import oc.adventure.commande.ms.entity.contract.Session;
import oc.adventure.commande.ms.entity.contract.User;
import oc.adventure.commande.ms.entity.impl.CommandeImpl;
import oc.adventure.commande.ms.entity.impl.CommandeSessionImpl;
import oc.adventure.commande.ms.entity.impl.UserImpl;
import oc.adventure.commande.ms.repository.contract.UserRepository;
import oc.adventure.commande.ms.service.contract.UserService;
import oc.adventure.shared.components.dto.commande.commande.CommandeOutDto;
import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionOutDto;
import oc.adventure.shared.components.dto.commande.session.SessionOutDto;
import oc.adventure.shared.components.dto.commande.user.UserInDto;
import oc.adventure.shared.components.dto.commande.user.UserOutDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository<User> userRepository;

    @Autowired
    public JMapper<UserImpl, UserOutDto> outDtoToUserMapper;

    @Autowired
    public JMapper<UserOutDto, UserImpl> userToOutDtoMapper;

    @Autowired
    public JMapper<UserImpl, UserInDto> inDtoToUserMapper;

    @Autowired
    public JMapper<UserInDto, UserImpl> userToInDtoMapper;

    @Autowired
    public JMapper<CommandeImpl, CommandeOutDto> outDtoToCommandeJMapper;

    @Autowired
    public JMapper<CommandeOutDto, CommandeImpl> commandeToOoutDtoJMapper;

    @Autowired
    public JMapper<CommandeSessionImpl, CommandeSessionOutDto> outDtoToCommandeSessionMapper;

    @Autowired
    public JMapper<CommandeSessionOutDto, CommandeSessionImpl> commandeSessionToOutDtoMapper;

    @Override
    public UserOutDto findByExternalId(String externalId){
        User user = this.userRepository.findByExtId(externalId);
        UserOutDto userOutDto = userToOutDtoMapper.getDestination((UserImpl) user);
        Set<Commande> commandeList = user.getCommandes();
        List<CommandeOutDto> commandeOutDtos = new ArrayList<>();
        for(Commande commande : commandeList){
            CommandeOutDto commandeOutDto = commandeToOoutDtoJMapper.getDestination((CommandeImpl) commande);
            List<CommandeSessionOutDto> sessionOutDtos = new ArrayList<>();
            Set<CommandeSession> commandeSessionSet = commande.getCommandeSessions();
            for(CommandeSession commandeSession: commandeSessionSet){
                Session session = commandeSession.getSession();
                CommandeSessionOutDto sessionOutDto = this.commandeSessionToOutDtoMapper.getDestination((CommandeSessionImpl) commandeSession);
                sessionOutDto.setAdventureName(session.getAdventure().getNameAdventure());
                sessionOutDto.setStartDate(session.getStartDate());
                sessionOutDto.setEndDate(session.getEndDate());

                sessionOutDtos.add(sessionOutDto);
            }
            commandeOutDto.getCommandeSessionOutDtoList().addAll(sessionOutDtos);
            commandeOutDtos.add(commandeOutDto);
        }
        userOutDto.getCommandeList().addAll(commandeOutDtos);
        return userOutDto;
    }

    @Override
    public UserInDto create(UserInDto userInDto){
        return this.userToInDtoMapper.getDestination((UserImpl) this.userRepository.create(this.inDtoToUserMapper.getDestination(userInDto)));
    }

    @Override
    public UserInDto update(UserInDto userInDto){
        User user = this.userRepository.findByExtId(userInDto.getExternalId());
        if (user == null)
            return null;
        return this.userToInDtoMapper.getDestination((UserImpl) this.userRepository.merge(user, userInDto.getFirstName(), userInDto.getLastName()));
    }

    @Override
    public boolean delete(String externalId){
        User user = this.userRepository.findByExtId(externalId);
        if(user == null)
            return false;
        this.userRepository.delete(user);
        return this.userRepository.findByExtId(externalId) == null;
    }

}
