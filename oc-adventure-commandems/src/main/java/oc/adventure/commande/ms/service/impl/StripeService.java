package oc.adventure.commande.ms.service.impl;

import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class StripeService {

    @PostConstruct
    public void init() {
        Stripe.apiKey = "sk_test_Vpviu0YakD7UfYLlUKtVVppb00nqO5UENo";
    }
    public Charge charge(double amount, String currency, String description, String stripeToken)
      throws AuthenticationException, InvalidRequestException,
            APIConnectionException, CardException, APIException {
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount",(int) amount * 100);
        chargeParams.put("currency", currency);
        chargeParams.put("description", description);
        chargeParams.put("source", stripeToken);
        return Charge.create(chargeParams);
    }
}