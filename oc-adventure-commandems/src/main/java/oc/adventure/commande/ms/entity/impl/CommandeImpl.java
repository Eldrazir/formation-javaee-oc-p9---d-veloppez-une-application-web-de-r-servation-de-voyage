package oc.adventure.commande.ms.entity.impl;

import oc.adventure.commande.ms.entity.contract.Commande;
import oc.adventure.commande.ms.entity.contract.CommandeSession;
import oc.adventure.commande.ms.entity.contract.User;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Commande")
@NamedQueries({
        @NamedQuery(
                name = CommandeImpl.QN.FIND_BY_EXTERNAL_ID,
                query = "SELECT c FROM CommandeImpl c " +
                        "WHERE c.externalId = :externalId"
        )
})
public class CommandeImpl implements Commande {

    public static class QN {
        public static final String FIND_BY_EXTERNAL_ID = "CommandeImpl.findByExtId";

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBUtils.ID)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;

    @Column(name = DBUtils.TOTAL_COST_DB)
    private double totalCost;

    @Column(name = DBUtils.STRIPE_EMAIL_DB)
    private String stripeEmail;

    @Column(name = DBUtils.STRIPE_TOKEN_DB, unique = true)
    private String stripeToken;

    @Column(name = DBUtils.CURRENCY)
    private String currency;

    @Column(name = DBUtils.FULL_ADDRESS_DB)
    private String fullAddress;

    @Column(name = DBUtils.PAIEMENT_DATE)
    private LocalDateTime paiementDate;

    @ManyToOne(targetEntity = UserImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "commande", fetch = FetchType.LAZY, targetEntity = CommandeSessionImpl.class)
    private Set<CommandeSession> commandeSessions = new HashSet<>();

    public CommandeImpl() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getStripeEmail() {
        return stripeEmail;
    }

    public void setStripeEmail(String stripeEMail) {
        this.stripeEmail = stripeEMail;
    }

    @Override
    public String getStripeToken() {
        return stripeToken;
    }

    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    @Override
    public LocalDateTime getPaiementDate() {
        return paiementDate;
    }

    public void setPaiementDate(LocalDateTime paiementDate) {
        this.paiementDate = paiementDate;
    }

    @Override
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public Set<CommandeSession> getCommandeSessions() {
        return commandeSessions;
    }

    @Override
    public void addCommandeSession(CommandeSession commandeSession){
        this.commandeSessions.add(commandeSession);
        ((CommandeSessionImpl) commandeSession).setCommande(this);
    }

    public void setCommandeSessions(Set<CommandeSession> commandeSessions) {
        this.commandeSessions = commandeSessions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommandeImpl)) return false;
        CommandeImpl commande = (CommandeImpl) o;
        return Objects.equals(getExternalId(), commande.getExternalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalId());
    }
}
