package oc.adventure.commande.ms.repository.contract;

import oc.adventure.commande.ms.entity.contract.Session;

public interface SessionRepository {
    Session findByExternalId(String externalId);
}
