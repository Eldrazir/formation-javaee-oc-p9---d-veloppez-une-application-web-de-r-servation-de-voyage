package oc.adventure.commande.ms.entity.contract;

import java.time.LocalDateTime;
import java.util.Set;

public interface Commande {
    Long getId();

    String getExternalId();

    double getTotalCost();

    String getStripeEmail();

    String getStripeToken();

    String getCurrency();

    String getFullAddress();

    LocalDateTime getPaiementDate();

    User getUser();

    Set<CommandeSession> getCommandeSessions();

    void addCommandeSession(CommandeSession commandeSession);
}
