package oc.adventure.commande.ms.entity.impl;


import oc.adventure.commande.ms.entity.contract.Adventure;
import oc.adventure.commande.ms.entity.contract.CommandeSession;
import oc.adventure.commande.ms.entity.contract.Session;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "session")
@NamedQuery(
        name = SessionImpl.QN.FIND_BY_EXTERNAL_ID,
        query = "SELECT s FROM SessionImpl s " +
                "WHERE s.externalId = :external_id"
)
public class SessionImpl implements Session {

    public static class QN {
        public static final String FIND_BY_EXTERNAL_ID = "SessionImpl.findByExtID";

        public static final String FIND_ALL = "SessionImpl.findAll";
        public static final String FIND_ALL_BY_START_DATE = "SessionImpl.findAllByStartDate";
        public static final String FIND_ALL_BY_END_DATE = "SessionImpl.findAllByEndDate";
        public static final String FIND_ALL_BY_LIMIT_BOOKING_DATE = "SessionImpl.findAllLimitBookingDate";
        public static final String FIND_ALL_BY_PRICE = "SessionImpl.findAllPrice";
        public static final String FIND_ALL_BY_TOTAL_AVAILABLE_PLACE = "SessionImpl.findAllTotalAvailablePlace";
        public static final String FIND_ALL_BY_ADVENTURE_ID = "SessionImpl.findAllByAdventureId";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBUtils.ID)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;

    @Column(name = DBUtils.START_DATE_DB)
    private LocalDate startDate;

    @Column(name = DBUtils.END_DATE_DB)
    private LocalDate endDate;

    @ManyToOne(targetEntity = AdventureImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = DBUtils.ADVENTURE_ID)
    private Adventure adventure;

    @OneToMany(targetEntity = CommandeSessionImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "session_id")
    private Set<CommandeSession> commandeSessions = new HashSet<>();

    public SessionImpl() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public Adventure getAdventure() {
        return adventure;
    }

    public void setAdventure(Adventure adventure) {
        this.adventure = adventure;
    }

    @Override
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @Override
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public Set<CommandeSession> getCommandeSessions() {
        return commandeSessions;
    }

    @Override
    public void addCommandeSession(CommandeSession commandeSession) {
        this.commandeSessions.add(commandeSession);
        ((CommandeSessionImpl) commandeSession).setSession(this);
    }

    public void setCommandeSessions(Set<CommandeSession> commandeSessions) {
        this.commandeSessions = commandeSessions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SessionImpl)) return false;
        SessionImpl session = (SessionImpl) o;
        return Objects.equals(getExternalId(), session.getExternalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalId());
    }
}
