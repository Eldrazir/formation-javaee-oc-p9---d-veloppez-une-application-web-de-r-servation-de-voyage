package oc.adventure.commande.ms.controller.config;

import com.googlecode.jmapper.JMapper;
import oc.adventure.commande.ms.entity.impl.CommandeImpl;
import oc.adventure.commande.ms.entity.impl.CommandeSessionImpl;
import oc.adventure.commande.ms.entity.impl.UserImpl;

import oc.adventure.shared.components.dto.commande.commande.CommandeInDto;
import oc.adventure.shared.components.dto.commande.commande.CommandeOutDto;
import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionInDto;
import oc.adventure.shared.components.dto.commande.commandesession.CommandeSessionOutDto;
import oc.adventure.shared.components.dto.commande.user.UserInDto;
import oc.adventure.shared.components.dto.commande.user.UserOutDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DtoJMapperInit {


    @Bean
    public JMapper<UserImpl, UserInDto> inDtoToUserMapper(){
        return new JMapper<>(UserImpl.class, UserInDto.class);
    }

    @Bean
    public JMapper<UserInDto, UserImpl> userToInDtoMapper(){
        return new JMapper<>(UserInDto.class, UserImpl.class);
    }

    @Bean
    public JMapper<UserImpl, UserOutDto> outDtoToUserMapper(){
        return new JMapper<>(UserImpl.class, UserOutDto.class);
    }

    @Bean
    public JMapper<UserOutDto, UserImpl> userToOutDtoMapper(){
        return new JMapper<>(UserOutDto.class, UserImpl.class);
    }

    @Bean
    public JMapper<CommandeImpl, CommandeOutDto> outDtoToCommandeMapper(){
        return new JMapper<>(CommandeImpl.class, CommandeOutDto.class);
    }

    @Bean
    public JMapper<CommandeOutDto, CommandeImpl> commandeToOutDtoMapper() {
        return new JMapper<>(CommandeOutDto.class, CommandeImpl.class);
    }

    @Bean
    public JMapper<CommandeImpl, CommandeInDto> inDtoToCommandeMapper(){
        return new JMapper<>(CommandeImpl.class, CommandeInDto.class);
    }

    @Bean
    public JMapper<CommandeInDto, CommandeImpl> commandeToInDtoMapper() {
        return new JMapper<>(CommandeInDto.class, CommandeImpl.class);
    }

    @Bean
    public JMapper<CommandeSessionImpl, CommandeSessionOutDto> outDtoToCommandeSessionMapper(){
        return new JMapper<>(CommandeSessionImpl.class, CommandeSessionOutDto.class);
    }

    @Bean
    public JMapper<CommandeSessionOutDto, CommandeSessionImpl> commandeSessionToOutDtoMapper(){
        return new JMapper<>(CommandeSessionOutDto.class, CommandeSessionImpl.class);
    }

    @Bean
    public JMapper<CommandeSessionImpl, CommandeSessionInDto> inDtoToCommandeSessionMapper(){
        return new JMapper<>(CommandeSessionImpl.class, CommandeSessionInDto.class);
    }

    @Bean
    public JMapper<CommandeSessionInDto, CommandeSessionImpl> commandeSessionToInDtoMapper(){
        return new JMapper<>(CommandeSessionInDto.class, CommandeSessionImpl.class);
    }

}
