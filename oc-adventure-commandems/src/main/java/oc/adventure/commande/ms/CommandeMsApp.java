package oc.adventure.commande.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "oc.adventure.commande.ms")
@EnableSwagger2
public class CommandeMsApp {
    public static void main( String[] args )
    {
        SpringApplication.run(CommandeMsApp.class, args);
    }
}
