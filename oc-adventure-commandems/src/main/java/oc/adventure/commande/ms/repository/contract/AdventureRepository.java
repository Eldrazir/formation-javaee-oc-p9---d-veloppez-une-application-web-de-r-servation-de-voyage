package oc.adventure.commande.ms.repository.contract;

import oc.adventure.commande.ms.entity.contract.Adventure;
import oc.adventure.shared.components.entity.contract.AdventureParent;
import oc.adventure.shared.components.repository.AdventureRepositoryParent;

public interface AdventureRepository<T extends AdventureParent> extends AdventureRepositoryParent<T> {


    Adventure findByNameAdventure(String nameAdventure);

    Adventure findByExtId(String extId);

    Adventure save(Adventure adventure);

    void delete(Adventure adventure);

    Adventure merge(Adventure adventure, String nameAdventure);
}
