package oc.adventure.commande.ms.entity.impl;

import oc.adventure.commande.ms.entity.contract.Commande;
import oc.adventure.commande.ms.entity.contract.User;
import oc.adventure.shared.components.entity.UserParentImpl;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class UserImpl extends UserParentImpl implements User {

    @OneToMany(targetEntity = CommandeImpl.class, fetch = FetchType.LAZY, mappedBy = "user")
    Set<Commande> commandes = new HashSet<>();

    public UserImpl() {
    }

    @Override
    public Set<Commande> getCommandes() {
        return commandes;
    }

    @Override
    public void addCommande(Commande commande) {
        this.commandes.add(commande);
        ((CommandeImpl)commande).setUser(this);
    }

    public void setCommandes(Set<Commande> commandes) {
        this.commandes = commandes;
    }


}
