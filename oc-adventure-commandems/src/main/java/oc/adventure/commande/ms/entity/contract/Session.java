package oc.adventure.commande.ms.entity.contract;

import java.time.LocalDate;
import java.util.Set;

public interface Session {
    Long getId();

    String getExternalId();

    Adventure getAdventure();

    LocalDate getStartDate();

    LocalDate getEndDate();

    Set<CommandeSession> getCommandeSessions();

    void addCommandeSession(CommandeSession commandeSession);
}
