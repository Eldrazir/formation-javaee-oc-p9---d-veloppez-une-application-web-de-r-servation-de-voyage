package oc.adventure.commande.ms.repository.impl;

import oc.adventure.commande.ms.entity.contract.Session;
import oc.adventure.commande.ms.entity.impl.SessionImpl;
import oc.adventure.commande.ms.repository.contract.SessionRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class SessionRepositoryImpl implements SessionRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Session findByExternalId(String externalId){
        TypedQuery<Session> sessionTypedQuery = this.entityManager.createNamedQuery(SessionImpl.QN.FIND_BY_EXTERNAL_ID, Session.class)
                .setParameter(DBUtils.EXTERNAL_ID_DB, externalId);

        return JPAUtils.getSingleResult(sessionTypedQuery);

    }
}
