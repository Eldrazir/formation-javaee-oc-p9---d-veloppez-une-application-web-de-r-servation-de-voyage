package oc.adventure.commande.ms.repository.contract;

import oc.adventure.commande.ms.entity.contract.User;
import oc.adventure.shared.components.repository.UserRepositoryParent;

public interface UserRepository<T extends User> extends UserRepositoryParent<T> {


    User create(User user);

    User merge(User user, String firstName, String lastName);

    void delete(User user);
}
