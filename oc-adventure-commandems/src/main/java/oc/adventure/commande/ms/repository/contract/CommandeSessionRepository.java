package oc.adventure.commande.ms.repository.contract;

import oc.adventure.commande.ms.entity.contract.CommandeSession;

public interface CommandeSessionRepository {
    CommandeSession save(CommandeSession commandeSession);
}
