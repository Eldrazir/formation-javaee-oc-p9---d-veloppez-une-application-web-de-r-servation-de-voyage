package oc.adventure.commande.ms.entity.impl;

import oc.adventure.commande.ms.entity.contract.Commande;
import oc.adventure.commande.ms.entity.contract.CommandeSession;
import oc.adventure.commande.ms.entity.contract.Session;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "commande_session")
@NamedQueries({
        @NamedQuery(
                name = CommandeSessionImpl.QN.FIND_BY_EXTERNAL_ID,
                query = "SELECT cs FROM CommandeSessionImpl cs " +
                        "WHERE cs.externalId = :externalId"
        )
})
public class CommandeSessionImpl implements CommandeSession {

    public static class QN {
        public static final String FIND_BY_EXTERNAL_ID = "CommandeSessionImpl.findByExtId";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBUtils.ID)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private float price;

    @ManyToOne(targetEntity = CommandeImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "commande_id")
    private Commande commande;

    @ManyToOne(targetEntity = SessionImpl.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "session_id")
    private Session session;

    public CommandeSessionImpl() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @Override
    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommandeSessionImpl)) return false;
        CommandeSessionImpl that = (CommandeSessionImpl) o;
        return Objects.equals(externalId, that.externalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(externalId);
    }
}
