package oc.adventure.commande.ms.repository.impl;


import oc.adventure.commande.ms.entity.contract.CommandeSession;
import oc.adventure.commande.ms.entity.impl.CommandeSessionImpl;
import oc.adventure.commande.ms.repository.contract.CommandeSessionRepository;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CommandeSessionRepositoryImpl implements CommandeSessionRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public CommandeSession save(CommandeSession commandeSession){
        ((CommandeSessionImpl)commandeSession).setExternalId(JPAUtils.GetBase62(8));
        this.entityManager.persist(commandeSession);
        return commandeSession;
    }

}
