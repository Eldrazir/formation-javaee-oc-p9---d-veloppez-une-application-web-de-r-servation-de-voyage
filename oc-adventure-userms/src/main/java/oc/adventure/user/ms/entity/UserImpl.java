package oc.adventure.user.ms.entity;


import oc.adventure.shared.components.entity.UserParentImpl;
import oc.adventure.shared.components.entity.contract.UserParent;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.entity.contract.User;
import oc.adventure.utils.DBUtils;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = DBUtils.USER)
@NamedQueries({
        @NamedQuery(
                name = UserImpl.QN.FIND_BY_PHONE_NUMBER,
                query = UserImpl.QN.SELECT_FROM +
                        "WHERE u.phoneNumber = :phone_number"
        ),
        @NamedQuery(
                name = UserImpl.QN.FIND_BY_MAIL,
                query = UserImpl.QN.SELECT_FROM +
                        "WHERE  u.mail = :mail"
        ),

})
public class UserImpl extends UserParentImpl implements User {

    public static class QN extends UserParentImpl.QNP {
        public static final String FIND_BY_PHONE_NUMBER = "UserImpl.findByPhoneNumber";
        public static final String FIND_BY_MAIL = "UserImpl.findByMail";
    }

    @Column(name = DBUtils.PHONE_NUMBER_DB, unique = true)
    private String phoneNumber;

    @Column(name = DBUtils.MAIL, unique = true, nullable = false)
    private String mail;

    @Column(name = DBUtils.PASSWORD_DB)
    private String password;

    @Column(name = DBUtils.IS_VALID)
    private boolean isValid;

    @Column(name = DBUtils.VALIDATION_TOKEN, length = 128)
    private String validationToken;

    @ManyToMany(targetEntity = RoleImpl.class, fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Set<Role> roleList = new HashSet<Role>();

    public UserImpl() {
    }

    @Override
    public void addRole(Role role){
        role.getUserList().add(this);
        this.roleList.add(role);
    }

    @Override
    public void removeRole(Role role){
        role.getUserList().remove(this);
        this.roleList.remove(role);
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Set<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(Set<Role> roleList) {
        this.roleList = roleList;
    }

    @Override
    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    @Override
    public boolean getIsValid() {
        return isValid;
    }

    @Override
    public void setIsValid(boolean valid) {
        isValid = valid;
    }

    @Override
    public String getValidationToken() {
        return validationToken;
    }

    public void setValidationToken(String validationToken) {
        this.validationToken = validationToken;
    }
}
