package oc.adventure.user.ms.entity;

import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.entity.contract.User;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = DBUtils.ROLE)
@NamedQueries({
        @NamedQuery(
                name = RoleImpl.QN.FIND_ALL_ORDER_NAME_DESC,
                query = RoleImpl.QN.SELECT_FROM +
                        RoleImpl.QN.ORDER_NAME_DESC
        ),
        @NamedQuery(
                name = RoleImpl.QN.FIND_ALL_BY_NAME_LIKE_ORDER_NAME_DESC,
                query = RoleImpl.QN.SELECT_FROM +
                        "WHERE ro.name LIKE :name_like " +
                        RoleImpl.QN.ORDER_NAME_DESC
        ),
        @NamedQuery(
                name = RoleImpl.QN.FIND_ALL_ORDER_NAME_ASC,
                query = RoleImpl.QN.SELECT_FROM +
                        RoleImpl.QN.ORDER_NAME_ASC
        ),
        @NamedQuery(
                name = RoleImpl.QN.FIND_ALL_BY_NAME_LIKE_ORDER_NAME_ASC,
                query = RoleImpl.QN.SELECT_FROM +
                        "WHERE ro.name LIKE :name_like " +
                        RoleImpl.QN.ORDER_NAME_ASC
        ),
        @NamedQuery(
                name = RoleImpl.QN.FIND_BY_NAME,
                query = RoleImpl.QN.SELECT_FROM +
                        "WHERE ro.name = :name"
        ),
        @NamedQuery(
                name = RoleImpl.QN.FIND_BY_EXTERNAL_ID,
                query = RoleImpl.QN.SELECT_FROM +
                        "WHERE ro.externalId = :external_id"
        )
})
public class RoleImpl implements Role {

    public static class QN {
        public static final String FIND_ALL_ORDER_NAME_DESC = "RoleImpl.findAllOrderNameDesc";
        public static final String FIND_ALL_BY_NAME_LIKE_ORDER_NAME_DESC = "RoleImpl.findAllByNameLikeOrderNameDesc";

        public static final String FIND_ALL_ORDER_NAME_ASC = "RoleImpl.findAllOrderNameAsc";
        public static final String FIND_ALL_BY_NAME_LIKE_ORDER_NAME_ASC = "RoleImpl.findAllByNameLikeOrderNameAsc";

        public static final String FIND_BY_NAME = "RoleImpl.findByName";
        public static final String FIND_BY_EXTERNAL_ID = "RoleImpl.findByExternalId";

        public static final String SELECT_FROM = "SELECT ro FROM RoleImpl ro ";

        public static final String ORDER_NAME_DESC = "ORDER BY ro.name DESC";
        public static final String ORDER_NAME_ASC = "ORDER BY ro.name ASC";

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBUtils.ID)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;

    @Column(name = DBUtils.NAME, unique = true)
    private String name;

    @ManyToMany(mappedBy = "roleList", targetEntity = UserImpl.class, fetch = FetchType.LAZY)
    private Set<User> userList = new HashSet<>();

    public RoleImpl() {
    }

    @Override
    public void addUser(User user){
        user.getRoleList().add(this);
        this.userList.add(user);
    }

    @Override
    public void removeUser(User user){
        user.removeRole(this);
        this.userList.remove(user);
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Set<User> getUserList() {
        return userList;
    }

    public void setUserList(Set<User> vets) {
        this.userList = vets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoleImpl)) return false;
        RoleImpl role = (RoleImpl) o;
        return getExternalId().equals(role.getExternalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalId());
    }
}
