package oc.adventure.user.ms.repository.contract;

import oc.adventure.shared.components.repository.UserRepositoryParent;
import oc.adventure.user.ms.entity.UserImpl;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.entity.contract.User;

import java.util.List;

public interface UserRepository<T extends User> extends UserRepositoryParent<T> {

    T findByPhoneNumber(String phoneNumber);

    T findByMail(String mail);

    List<T> findAllByFirstNameLikeOrderLastNameDesc(String firstName, int pageNb, int nbPerPage);

    List<T> findAllByLastNameOrderLikeLastNameDesc(String lastName, int pageNb, int nbPerPage);

    List<T> findAllByFirstNameOrderLastNameAsc(String firstName, int pageNb, int nbPerPage);

    List<T> findAllByLastNameOrderLastNameAsc(String lastName, int pageNb, int nbPerPage);

    User save(T user);

    void delete(T User);

    User merge(T User, String firstName, String lastName, String phoneNumber, String mail);

    boolean hasRole(T User, Role roleToTest);

    void validate(T user, boolean b);
}
