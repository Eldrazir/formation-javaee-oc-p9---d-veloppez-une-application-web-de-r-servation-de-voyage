package oc.adventure.user.ms.service.contract;

import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import oc.adventure.shared.components.dto.user.ExistsDto;
import oc.adventure.shared.components.dto.user.UserRegisterDto;
import oc.adventure.shared.components.dto.user.UserOutDto;
import oc.adventure.shared.components.dto.user.UserRegisterLightDto;

import java.util.List;

public interface UserService {
    UserOutDto findByExternalId(String externalId);

    UserOutDto findByMail(String mail);

    AuthBodyOutDto login(String userMail);

    UserOutDto findByPhone(String phone);

    List<UserOutDto> findAll(int pageNb, int nbPerPage);

    ExistsDto existingMail(String mail);

    ExistsDto existingPhone(String phone);

    UserOutDto register(UserRegisterDto userRegisterDto);

    UserOutDto update(UserRegisterLightDto userOutDto, String externalId);

    boolean delete(String externalId);

    ExistsDto validateAccompte(String validationToken, String externalId);
}
