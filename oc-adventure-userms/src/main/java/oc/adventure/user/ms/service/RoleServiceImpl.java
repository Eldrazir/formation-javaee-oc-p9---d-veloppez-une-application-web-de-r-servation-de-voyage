package oc.adventure.user.ms.service;

import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.role.RoleInDto;
import oc.adventure.shared.components.dto.role.RoleOutDto;
import oc.adventure.user.ms.entity.RoleImpl;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.repository.contract.RoleRepository;
import oc.adventure.user.ms.service.contract.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional(propagation = Propagation.MANDATORY)
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    JMapper<RoleOutDto, RoleImpl> roleToDtoMapper;

    @Autowired
    JMapper<RoleImpl, RoleOutDto> dtoToRoleMapper;

    @Autowired
    JMapper<RoleInDto, RoleImpl> roleToInDtoMapper;

    @Autowired
    JMapper<RoleImpl, RoleInDto> inDtoToRoleMapper;

    @Override
    public List<RoleOutDto> findAll(int pageNb, int nbPerPage){
        List<Role> roleList = this.roleRepository.findAllOrderNameDesc(pageNb, nbPerPage);
        return roleList.stream().map(x -> roleToDtoMapper.getDestination((RoleImpl)x)).collect(Collectors.toList());
    }

    @Override
    public RoleOutDto findByName(String name){
        return roleToDtoMapper.getDestination((RoleImpl) this.roleRepository.findByName(name));
    }

    @Override
    public RoleOutDto findByExternalId(String externalId){
        return roleToDtoMapper.getDestination((RoleImpl) this.roleRepository.findByExternalId(externalId));
    }

    @Override
    public RoleOutDto create(RoleInDto roleInDto){
        Role role = this.roleRepository.save(inDtoToRoleMapper.getDestination(roleInDto));
        return this.roleToDtoMapper.getDestination((RoleImpl) role);
    }

    @Override
    public RoleOutDto merge(RoleOutDto roleInDto){
        Role role = this.roleRepository.findByExternalId(roleInDto.getExternalId());
        role = this.roleRepository.merge(role, roleInDto.getName());
        return this.roleToDtoMapper.getDestination((RoleImpl) role);
    }

    @Override
    public boolean delete(String externalId){
        Role role = this.roleRepository.findByExternalId(externalId);
        role.getUserList().forEach(x -> x.removeRole(role));
        this.roleRepository.delete(role);
        return (this.roleRepository.findByExternalId(externalId) == null);
    }

}
