package oc.adventure.user.ms.repository.contract;

import oc.adventure.user.ms.entity.contract.Role;

import java.util.List;

public interface RoleRepository {
    Role find(int id);

    Role findByName(String name);

    Role findByExternalId(String externalId);

    List<Role> findAllOrderNameDesc(int pageNb, int nbPerPage);

    List<Role> findAllOrderNameAsc(int pageNb, int nbPerPage);

    Role save(Role role);

    Role merge(Role role, String name);

    void delete(Role role);
}
