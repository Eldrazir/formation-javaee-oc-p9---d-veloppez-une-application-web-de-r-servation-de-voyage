package oc.adventure.user.ms.service.contract;

import oc.adventure.shared.components.dto.role.RoleInDto;
import oc.adventure.shared.components.dto.role.RoleOutDto;

import java.util.List;

public interface RoleService {
    List<RoleOutDto> findAll(int pageNb, int nbPerPage);

    RoleOutDto findByName(String name);

    RoleOutDto findByExternalId(String externalId);

    RoleOutDto create(RoleInDto roleInDto);

    RoleOutDto merge(RoleOutDto roleInDto);

    boolean delete(String externalId);
}
