package oc.adventure.user.ms.service.contract;

public interface EmailService {
    void sendSimpleMessage(
            String to, String subject, String text);
}
