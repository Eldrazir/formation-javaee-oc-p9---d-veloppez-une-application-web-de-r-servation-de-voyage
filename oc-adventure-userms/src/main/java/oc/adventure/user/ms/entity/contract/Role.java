package oc.adventure.user.ms.entity.contract;

import java.util.Set;

public interface Role {
    void addUser(User user);

    void removeUser(User user);

    Long getId();

    String getExternalId();

    String getName();

    Set<User> getUserList();
}
