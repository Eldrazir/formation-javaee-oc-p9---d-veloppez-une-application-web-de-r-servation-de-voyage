package oc.adventure.user.ms.controller.config;

import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.role.RoleInDto;
import oc.adventure.shared.components.dto.role.RoleOutDto;
import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import oc.adventure.shared.components.dto.user.UserRegisterDto;
import oc.adventure.shared.components.dto.user.UserOutDto;
import oc.adventure.user.ms.entity.RoleImpl;
import oc.adventure.user.ms.entity.UserImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class DtoJMapperInit {

    @Bean
    JMapper<RoleOutDto, RoleImpl> roleToDtoMapper(){
        return new JMapper<>(RoleOutDto.class, RoleImpl.class);
    }

    @Bean
    JMapper<RoleImpl, RoleOutDto> roleDtoToRoleMapper(){
        return new JMapper<>(RoleImpl.class, RoleOutDto.class);
    }

    @Bean
    JMapper<RoleInDto, RoleImpl> roleToInDtoMapper(){
        return new JMapper<>(RoleInDto.class, RoleImpl.class);
    }

    @Bean
    JMapper<RoleImpl, RoleInDto> roleInDtoToRoleMapper(){
        return new JMapper<>(RoleImpl.class, RoleInDto.class);
    }

    @Bean
    JMapper<UserOutDto, UserImpl> userToDtoMapper(){
        return new JMapper<>(UserOutDto.class, UserImpl.class);
    }

    @Bean
    JMapper<UserImpl, UserOutDto> dtoOutToUserMapper(){
        return new JMapper<>(UserImpl.class, UserOutDto.class);
    }

    @Bean
    JMapper<UserImpl, AuthBodyOutDto> authBodyOutDtoUserMapper(){
        return new JMapper<>(UserImpl.class, AuthBodyOutDto.class);
    }

    @Bean
    JMapper<AuthBodyOutDto , UserImpl> userToAuthBodyOutDtoMapper(){
        return new JMapper<>(AuthBodyOutDto.class,  UserImpl.class);
    }

    @Bean
    JMapper<UserImpl, UserRegisterDto> dtoInToUserMapper(){
        return new JMapper<>(UserImpl.class, UserRegisterDto.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

