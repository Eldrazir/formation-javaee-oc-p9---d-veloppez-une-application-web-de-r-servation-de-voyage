package oc.adventure.user.ms.controller;


import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import oc.adventure.shared.components.dto.user.ExistsDto;
import oc.adventure.shared.components.dto.user.UserRegisterDto;
import oc.adventure.shared.components.dto.user.UserOutDto;
import oc.adventure.shared.components.dto.user.UserRegisterLightDto;
import oc.adventure.user.ms.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ms/user/")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(value    = "/",
            produces = "application/json")
    @ResponseBody
    public List<UserOutDto> getAll(@RequestParam int pageNb, @RequestParam int nbPerPage){
        return this.userService.findAll(pageNb, nbPerPage);
    }

    @GetMapping(value    = "/{externalId}",
            produces = "application/json")
    @ResponseBody
    public UserOutDto getByExternalId(@PathVariable("externalId") String externalId){
        return this.userService.findByExternalId(externalId);
    }

    @GetMapping(value    = "mail/{userMail}",
            produces = "application/json")
    @ResponseBody
    public UserOutDto getByEmail(@PathVariable("userMail") String userMail){
        return this.userService.findByMail(userMail);
    }

    @GetMapping(value    = "login/{userMail}",
            produces = "application/json")
    @ResponseBody
    public AuthBodyOutDto login(@PathVariable ("userMail") String userMail){

        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ LOGIN CONTROLLER");
        return this.userService.login(userMail);
    }

    @GetMapping(value    = "phone/{userPhone}",
            produces = "application/json")
    @ResponseBody
    public UserOutDto getByPhone(@PathVariable("userPhone") String userPhone){
        return this.userService.findByPhone(userPhone);
    }

    @GetMapping(value    = "exists/phone/{userPhone}",
            produces = "application/json")
    @ResponseBody
    public ExistsDto existsPhone(@PathVariable("userPhone") String userPhone){
        return this.userService.existingPhone(userPhone);
    }

    @GetMapping(value    = "exists/mail/{userMail}",
            produces = "application/json")
    @ResponseBody
    public ExistsDto existsMail(@PathVariable("userMail") String userMail){
        return this.userService.existingMail(userMail);
    }

    @PostMapping(value    = "register/",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserOutDto register (@RequestBody UserRegisterDto userRegisterDto){
        userRegisterDto.setPassword(this.passwordEncoder.encode(userRegisterDto.getPassword()));
        return this.userService.register(userRegisterDto);
    }

    @GetMapping(value = "validate/",
            produces = "application/json"
    )
    public ExistsDto validateAccompte(@RequestParam(name = "ValidationToken") String validationToken, @RequestParam(name = "externalId") String externalId){
        return this.userService.validateAccompte(validationToken, externalId);
    }

    @PutMapping(value    = "update/{externalId}",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserOutDto update(@RequestBody UserRegisterLightDto userOutDto, @PathVariable("externalId") String externalId){
        return this.userService.update(userOutDto, externalId );
    }

    @DeleteMapping(value    = "delete/{externalId}",
            produces = "application/json")
    @ResponseBody
    public boolean delete(@PathVariable("externalId") String externalId){
        return this.userService.delete(externalId);
    }

}
