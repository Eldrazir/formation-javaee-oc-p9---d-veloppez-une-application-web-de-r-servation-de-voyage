package oc.adventure.user.ms.controller.config;

import oc.adventure.user.ms.entity.RoleImpl;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.repository.contract.RoleRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Autowired
    private RoleRepository roleRepository;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        createRoleIfNotFound(DBUtils.USER_STANDARD);
        createRoleIfNotFound(DBUtils.ROLE_ADMIN);
        alreadySetup = true;
    }


    private Role createRoleIfNotFound(String name) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new RoleImpl();
            ((RoleImpl)role).setExternalId(JPAUtils.GetBase62(8));
            ((RoleImpl)role).setName(name);
            roleRepository.save(role);
        }
        return role;
    }

}
