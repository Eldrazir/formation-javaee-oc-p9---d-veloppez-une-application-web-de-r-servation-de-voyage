package oc.adventure.user.ms.repository;

import oc.adventure.shared.components.repository.UserRepositoryParentImpl;
import oc.adventure.user.ms.entity.UserImpl;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.entity.contract.User;
import oc.adventure.user.ms.repository.contract.UserRepository;
import oc.adventure.utils.JPAUtils;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class UserRepositoryImpl extends UserRepositoryParentImpl<User> implements UserRepository<User> {

    @Override
    public User findByPhoneNumber(String phoneNumber){
        TypedQuery<User> UserTypedQuery = entityManager.createNamedQuery(UserImpl.QN.FIND_BY_PHONE_NUMBER, User.class).setParameter("phone_number", phoneNumber);
        User User = JPAUtils.getSingleResult(UserTypedQuery);
        if(User != null)
            User.getRoleList();
        return User;
    }

    @Override
    public User findByMail(String mail){
        TypedQuery<User> UserTypedQuery = this.entityManager.createNamedQuery(UserImpl.QN.FIND_BY_MAIL, User.class)
                .setParameter("mail", mail);
        return JPAUtils.getSingleResult(UserTypedQuery);
    }

    @Override
    public List<User> findAllByFirstNameLikeOrderLastNameDesc(String firstName, int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(UserImpl.QN.FIND_ALL_BY_FIRST_NAME_LIKE_ORDER_LAST_NAME_DESC, User.class)
                .setParameter("first_name_like", "%"+firstName+"%")
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public List<User> findAllByLastNameOrderLikeLastNameDesc(String lastName, int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(UserImpl.QN.FIND_ALL_BY_LAST_NAME_LIKE_ORDER_LAST_NAME_DESC, User.class)
                .setParameter("last_name_like", "%"+lastName+"%")
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public List<User> findAllByFirstNameOrderLastNameAsc(String firstName, int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(UserImpl.QN.FIND_ALL_BY_FIRST_NAME_LIKE_ORDER_LAST_NAME_ASC, User.class)
                .setParameter("first_name_like", "%"+firstName+"%")
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public List<User> findAllByLastNameOrderLastNameAsc(String lastName, int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(UserImpl.QN.FIND_ALL_BY_LAST_NAME_LIKE_ORDER_LAST_NAME_ASC, User.class)
                .setParameter("last_name_like", lastName)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public User save(User user){
        User UserFound = this.findByPhoneNumber(user.getPhoneNumber());
        if(UserFound != null)
            return null;
        UserFound = this.findByMail(user.getMail());
        if(UserFound != null)
            return null;
        String externalId = JPAUtils.GetBase62(8);
        User UserFoundByExternal = this.findByExtId(externalId);
        while(UserFoundByExternal != null){
            externalId = JPAUtils.GetBase62(8);
            UserFoundByExternal = this.findByExtId(externalId);
        }
        ((UserImpl) user).setExternalId(externalId);
        ((UserImpl) user).setValid(false);
        ((UserImpl) user).setValidationToken(JPAUtils.GetBase62(128));
        this.entityManager.persist(user);
        return user;
    }

    @Override
    public void delete(User User){
        if(!this.entityManager.contains(User))
            this.entityManager.merge(User);

        this.entityManager.remove(User);
    }


    @Override // update
    public User merge(User user, String firstName, String lastName, String phoneNumber, String mail){
        User UserFound = this.findByPhoneNumber(phoneNumber);
        if(UserFound != null && !user.equals(UserFound))
            return null;

        if(!this.entityManager.contains(user))
            this.entityManager.merge(user);

        ((UserImpl) user).setFirstName(firstName);
        ((UserImpl) user).setLastName(lastName);
        ((UserImpl) user).setPhoneNumber(phoneNumber);
        ((UserImpl) user).setMail(mail);
        return user;
    }

    
    @Override
    public boolean hasRole(User user, Role roleToTest){
        Collection<Role> roles = user.getRoleList();
        for(Role role: roles){
            if(role.getName().equals(roleToTest.getName())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void validate(User user, boolean b) {
        if(!this.entityManager.contains(user))
            this.entityManager.merge(user);
        ((UserImpl) user).setValid(true);
    }


    @Override
    public Class<User> getEntityTClass() {
        return User.class;
    }
}
