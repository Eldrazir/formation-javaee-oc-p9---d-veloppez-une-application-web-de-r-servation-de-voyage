package oc.adventure.user.ms.service;


import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import oc.adventure.shared.components.dto.user.ExistsDto;
import oc.adventure.shared.components.dto.user.UserRegisterDto;
import oc.adventure.shared.components.dto.user.UserOutDto;
import oc.adventure.shared.components.dto.user.UserRegisterLightDto;
import oc.adventure.user.ms.entity.UserImpl;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.entity.contract.User;
import oc.adventure.user.ms.repository.contract.RoleRepository;
import oc.adventure.user.ms.repository.contract.UserRepository;
import oc.adventure.user.ms.service.contract.EmailService;
import oc.adventure.user.ms.service.contract.UserService;
import oc.adventure.utils.DBUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository<User> userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private JMapper<UserOutDto, UserImpl> userToDtoMapper;

    @Autowired
    private JMapper<UserImpl, UserOutDto> dtoOutToUserMapper;

    @Autowired
    private JMapper<UserImpl, UserRegisterDto> dtoInToUserMapper;

    @Autowired
    private JMapper<AuthBodyOutDto, UserImpl> authBodyOutDtoUserMapper;

    @Autowired
    private EmailService emailService;

    @Override
    public UserOutDto findByExternalId(String externalId){
        return userToDtoMapper.getDestination((UserImpl) this.userRepository.findByExtId(externalId));
    }

    @Override
    public UserOutDto findByMail(String mail){
        return userToDtoMapper.getDestination((UserImpl) this.userRepository.findByMail(mail));
    }

    @Override
    public AuthBodyOutDto login(String userMail){
        User user = this.userRepository.findByMail(userMail);
        AuthBodyOutDto authBodyOutDto = authBodyOutDtoUserMapper.getDestination((UserImpl) user);
        user.getRoleList().forEach( x -> authBodyOutDto.getRoleList().add(x.getName()));
        return authBodyOutDto;
    }

    @Override
    public UserOutDto findByPhone(String phone){
        return userToDtoMapper.getDestination((UserImpl) this.userRepository.findByPhoneNumber(phone));
    }

    @Override
    public List<UserOutDto> findAll(int pageNb, int nbPerPage){
        List<User> userList = this.userRepository.findAllOrderLastNameDesc(pageNb, nbPerPage);
        return userList.stream().map(x -> userToDtoMapper.getDestination((UserImpl) x)).collect(Collectors.toList()); // On traduit List<User> en List<UserDto>
    }

    @Override
    public ExistsDto existingMail(String mail){
        return new ExistsDto(this.userRepository.findByMail(mail) != null);
    }

    @Override
    public ExistsDto existingPhone(String phone){
        return new ExistsDto(this.userRepository.findByPhoneNumber(phone) != null);
    }

    @Override
    public UserOutDto register(UserRegisterDto userRegisterDto){
        User user =  this.userRepository.save(dtoInToUserMapper.getDestination(userRegisterDto));
        if(user == null)
            return null;
        Role userRole = this.roleRepository.findByName(DBUtils.USER_STANDARD);
        if(userRole == null)
            return null;
        user.addRole(userRole);
        userRole.addUser(user);
        String text = "Yo l'ami, \n";

        text += "Ton url : http://localhost:4200/validate/?ValidationToken=" + user.getValidationToken() + "&externalId=" + user.getExternalId();
        this.emailService.sendSimpleMessage(user.getMail(), "Valide ton email Babe !", text);
        return userToDtoMapper.getDestination((UserImpl) user);
    }

    @Override
    public UserOutDto update(UserRegisterLightDto userOutDto, String externalId){
        User user = this.userRepository.findByExtId(externalId);
        if(user == null)
            return null;
        return userToDtoMapper.getDestination((UserImpl) this.userRepository.merge(user, userOutDto.getFirstName(), userOutDto.getLastName(), userOutDto.getPhoneNumber(), userOutDto.getMail()));
    }

    @Override
    public boolean delete(String externalId){
        User user = this.userRepository.findByExtId(externalId);
        if(user == null)
            return false;
        this.userRepository.delete(user);
        user = this.userRepository.findByExtId(externalId);
        return (user == null);
    }

    @Override
    public ExistsDto validateAccompte(String validationToken, String externalId) {
        ExistsDto existsDto = new ExistsDto();
        User user = this.userRepository.findByExtId(externalId);
        if(user != null && user.getValidationToken().equals(validationToken)){
            existsDto.setExists(true);
            this.userRepository.validate(user, true);
        } else {
            existsDto.setExists(false);
        }
        return existsDto;
    }


}
