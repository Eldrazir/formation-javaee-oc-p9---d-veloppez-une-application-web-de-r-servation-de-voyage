package oc.adventure.user.ms.controller;



import oc.adventure.shared.components.dto.role.RoleInDto;
import oc.adventure.shared.components.dto.role.RoleOutDto;
import oc.adventure.user.ms.service.contract.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ms/user/role")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping(value    = "/",
            produces = "application/json")
    @ResponseBody
    public List<RoleOutDto> getAll(@RequestParam int pageNb, @RequestParam int nbPerPage){
        return this.roleService.findAll(pageNb, nbPerPage);
    }

    @GetMapping(value    = "/name/{name}",
            produces = "application/json")
    @ResponseBody
    public RoleOutDto getByName(@PathVariable("name") String name){
        return this.roleService.findByName(name);
    }

    @GetMapping(value    = "/externalid/{externalId}",
            produces = "application/json")
    @ResponseBody
    public RoleOutDto getByExternalId(@PathVariable("externalId") String externalId){
        return this.roleService.findByExternalId(externalId);
    }

    @PostMapping(value ="/create/",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public RoleOutDto create(@RequestBody RoleInDto roleInDto){
        return this.roleService.create(roleInDto);
    }

    @PutMapping(value = "/merge/",
                produces = "application/json",
                consumes = "application/json")
    @ResponseBody
    public RoleOutDto merge(@RequestBody RoleOutDto roleOutDto){
        return this.roleService.merge(roleOutDto);
    }

    @DeleteMapping(value = "/delete/{externalId}",
            produces = "application/json")
    @ResponseBody
    public boolean delete(@PathVariable("externalId") String externalId){
        return this.roleService.delete(externalId);
    }

}
