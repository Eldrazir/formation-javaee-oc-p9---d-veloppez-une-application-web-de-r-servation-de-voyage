package oc.adventure.user.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages = "oc.adventure.user.ms")
@EnableSwagger2
public class UserMsApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(UserMsApp.class, args);
    }
}
