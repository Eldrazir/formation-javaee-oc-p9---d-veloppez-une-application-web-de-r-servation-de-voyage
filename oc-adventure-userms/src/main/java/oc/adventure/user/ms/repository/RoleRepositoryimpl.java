package oc.adventure.user.ms.repository;

import oc.adventure.user.ms.entity.RoleImpl;
import oc.adventure.user.ms.entity.contract.Role;
import oc.adventure.user.ms.repository.contract.RoleRepository;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Set;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class RoleRepositoryimpl implements RoleRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Role find(int id){
        return this.entityManager.find(RoleImpl.class, id);
    }

    @Override
    public Role findByName(String name) {
        TypedQuery<Role> role = entityManager.createNamedQuery(RoleImpl.QN.FIND_BY_NAME, Role.class)
                .setParameter("name", name);
        return JPAUtils.getSingleResult(role);
    }

    @Override
    public Role findByExternalId(String externalId) {
        TypedQuery<Role> role = entityManager.createNamedQuery(RoleImpl.QN.FIND_BY_EXTERNAL_ID, Role.class)
                .setParameter("external_id", externalId);
        return JPAUtils.getSingleResult(role);
    }

    @Override
    public List<Role> findAllOrderNameDesc(int pageNb, int nbPerPage) {
        return entityManager.createNamedQuery(RoleImpl.QN.FIND_ALL_ORDER_NAME_DESC, Role.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public List<Role> findAllOrderNameAsc(int pageNb, int nbPerPage) {
        return entityManager.createNamedQuery(RoleImpl.QN.FIND_ALL_ORDER_NAME_ASC, Role.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public Role save(Role role) {
        if(this.findByName(role.getName()) != null)
            return null;
        String externalId = JPAUtils.GetBase62(8);
        ((RoleImpl) role).setExternalId(externalId);
        entityManager.persist(role);
        return role;
    }

    @Override
    public Role merge(Role role, String name){
        Role roleFound = this.findByName(name);
        if(roleFound != null && role.getId() != null && !role.getId().equals(roleFound.getId()))
            return null;
        if(!this.entityManager.contains(role))
            this.entityManager.merge(role);
        ((RoleImpl)role).setName(name);
        return role;
    }

    @Override
    public void delete(Role role){
        this.entityManager.remove(role);
    }
}
