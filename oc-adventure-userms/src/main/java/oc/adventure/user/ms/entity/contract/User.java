package oc.adventure.user.ms.entity.contract;

import oc.adventure.shared.components.entity.contract.UserParent;

import java.util.Set;

public interface User extends UserParent {
    void addRole(Role role);

    void removeRole(Role role);

    String getPhoneNumber();

    String getMail();

    String getPassword();

    Set<Role> getRoleList();

    boolean isValid();

    boolean getIsValid();

    void setIsValid(boolean valid);

    String getValidationToken();
}
