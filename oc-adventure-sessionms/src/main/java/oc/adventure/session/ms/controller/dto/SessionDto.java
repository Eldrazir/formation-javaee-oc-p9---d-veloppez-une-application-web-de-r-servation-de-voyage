package oc.adventure.session.ms.controller.dto;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.jmapper.annotations.JMap;

import oc.adventure.session.ms.entity.contract.Adventure;

public class SessionDto implements Serializable {

	@JMap // Pour indiquer qu'il doit automatiquement Mapper cet attribut avec l'attribut de même nom de l'entité User (hibernate)
	private String externalId;
	
	@JMap
	private Date startDate;
	
	@JMap
	private Date endDate;
	
	@JMap
	private Date limitBookingDate;
	
	@JMap
	private Double price;
	
	@JMap
	private Integer totalAvailablePlace;
	
	@JMap
	private String description;
	
	public SessionDto() {
		
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getLimitBookingDate() {
		return limitBookingDate;
	}

	public void setLimitBookingDate(Date limitBookingDate) {
		this.limitBookingDate = limitBookingDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getTotalAvailablePlace() {
		return totalAvailablePlace;
	}

	public void setTotalAvailablePlace(Integer totalAvailablePlace) {
		this.totalAvailablePlace = totalAvailablePlace;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
	
