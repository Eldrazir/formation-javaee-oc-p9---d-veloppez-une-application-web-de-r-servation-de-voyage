package oc.adventure.session.ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import oc.adventure.session.ms.controller.dto.SessionDto;
import oc.adventure.session.ms.service.contract.SessionService;

@RestController
@RequestMapping("/api/v1/ms/session")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SessionController {
	
	@Autowired
	private SessionService sessionService;
	
	@GetMapping(value = "/",
			produces = "application/json")	
	@ResponseBody
	public List<SessionDto> getAll(@RequestParam int pageNb, @RequestParam int nbPerPage){
		return this.sessionService.findAll(pageNb,nbPerPage);
	}
	
	@GetMapping(value = "/adventure/{externalId}",
			produces = "application/json")	
	@ResponseBody
	public List<SessionDto> getAllByAdventureId(@PathVariable("externalId") String adventureId, @RequestParam int pageNb, @RequestParam int nbPerPage){
		return this.sessionService.findAllByAdventureId(adventureId, pageNb, nbPerPage);
	}

}
