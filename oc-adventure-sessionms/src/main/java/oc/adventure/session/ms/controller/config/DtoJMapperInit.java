package oc.adventure.session.ms.controller.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.googlecode.jmapper.JMapper;

import oc.adventure.session.ms.controller.dto.AdventureDto;
import oc.adventure.session.ms.controller.dto.SessionDto;
import oc.adventure.session.ms.entity.AdventureImpl;
import oc.adventure.session.ms.entity.SessionImpl;

@Configuration
public class DtoJMapperInit {

    @Bean
    JMapper<SessionDto, SessionImpl> sessionToDtoMapper(){
        return new JMapper<>(SessionDto.class, SessionImpl.class);
    }
    
    @Bean
    JMapper<SessionImpl, SessionDto> sessionDtoToSessionMapper(){
        return new JMapper<>(SessionImpl.class, SessionDto.class);
    }
    
    @Bean
    JMapper<AdventureDto, AdventureImpl> adventureToDtoMapper(){
        return new JMapper<>(AdventureDto.class, AdventureImpl.class);
    }
    
    @Bean
    JMapper<AdventureImpl, AdventureDto> adventureDtoToAdventureMapper(){
        return new JMapper<>(AdventureImpl.class, AdventureDto.class);
    }

}

