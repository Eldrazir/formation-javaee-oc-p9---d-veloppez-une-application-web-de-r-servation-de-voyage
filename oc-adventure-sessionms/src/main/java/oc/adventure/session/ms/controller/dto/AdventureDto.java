package oc.adventure.session.ms.controller.dto;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.jmapper.annotations.JMap;

public class AdventureDto implements Serializable {
	
	@JMap // Pour indiquer qu'il doit automatiquement Mapper cet attribut avec l'attribut de même nom de l'entité User (hibernate)
	private String externalId;
	
	@JMap
	private String name;
	
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
