package oc.adventure.session.ms.entity.contract;

public interface Adventure {

	String getName();

	String getExternalId();

	Long getId();

}
