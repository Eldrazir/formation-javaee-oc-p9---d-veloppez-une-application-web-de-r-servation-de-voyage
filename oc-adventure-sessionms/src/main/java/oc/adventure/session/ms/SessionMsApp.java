package oc.adventure.session.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "oc.adventure.session.ms")
@EnableSwagger2
public class SessionMsApp 
{
	public static void main( String[] args )
    {
        SpringApplication.run(SessionMsApp.class, args);
    }
}
