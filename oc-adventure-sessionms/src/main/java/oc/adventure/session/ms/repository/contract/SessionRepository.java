package oc.adventure.session.ms.repository.contract;

import java.util.Date;
import java.util.List;

import oc.adventure.session.ms.entity.contract.Adventure;
import oc.adventure.session.ms.entity.contract.Session;

public interface SessionRepository {

	Session findByExternalId(String externalId);

	List<Session> findAll(int pageNb, int nbPerPage);

	Session merge(Session session, Date startDate, Date endDate, Date limiteBookingDate, Double price, Integer totalAvailablePlace, String description, Adventure adventure);

	void delete(Session session);

	Session save(Session session);

	List<Session> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage);

}
