package oc.adventure.session.ms.service.contract;

import java.util.List;

import oc.adventure.session.ms.controller.dto.SessionDto;
import oc.adventure.session.ms.entity.contract.Session;

public interface SessionService {

	Session findByExternalId(String externalId);

	List<SessionDto> findAll(int pageNb, int nbPerPage);
	
	List<SessionDto> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage);

}
