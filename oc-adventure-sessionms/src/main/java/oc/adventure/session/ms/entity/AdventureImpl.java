package oc.adventure.session.ms.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import oc.adventure.session.ms.entity.contract.Adventure;
import oc.adventure.session.ms.entity.contract.Session;
import oc.adventure.utils.DBUtils;

@Entity
@Table(name = DBUtils.ADVENTURE)
@NamedQueries({
	@NamedQuery(
            name = AdventureImpl.QN.FIND_BY_NAME,
            query = AdventureImpl.QN.SELECT_FROM +
                    "WHERE  a.name = :name_adventure"
    )
})
public class AdventureImpl implements Adventure {

	public static class QN {
		public static final String FIND_BY_EXTERNAL_ID = "AdventureImpl.findByExtID";
		public static final String FIND_BY_NAME = "AdventureImpl.findByName";
		
		public static final String FIND_ALL = "AdventureImpl.findAll";
		public static final String SELECT_FROM = "SELECT a FROM AdventureImpl a ";
	}
	
	@Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;
    
    @Column(name = DBUtils.NAME_ADVENTURE_DB)
    private String name;
    
    @OneToMany(mappedBy = "adventure", targetEntity = SessionImpl.class,  fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Session> sessionList = new HashSet<>();

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Session> getSessionList() {
		return sessionList;
	}

	public void setSessionList(Set<Session> sessionList) {
		this.sessionList = sessionList;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setAdventureName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(externalId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof AdventureImpl)) {
			return false;
		}
		AdventureImpl other = (AdventureImpl) obj;
		return Objects.equals(externalId, other.externalId);
	}
	
}
