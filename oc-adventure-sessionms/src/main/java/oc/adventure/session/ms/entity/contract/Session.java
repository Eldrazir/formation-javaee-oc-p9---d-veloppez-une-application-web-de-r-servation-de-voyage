package oc.adventure.session.ms.entity.contract;

import java.util.Date;

public interface Session {

	String getDescription();

	Integer getTotalAvailablePlace();

	Double getPrice();

	Date getLimitBookingDate();

	Date getEndDate();

	Date getStartDate();

	String getExternalId();

	Long getId();
	
	Adventure getAdventure();

}
