package oc.adventure.session.ms.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import oc.adventure.session.ms.entity.contract.Adventure;
import oc.adventure.session.ms.entity.contract.Session;
import oc.adventure.utils.DBUtils;

@Entity
@Table(name = DBUtils.SESSION)
@NamedQueries({
	@NamedQuery(
            name = SessionImpl.QN.FIND_ALL,
            query = "SELECT s FROM SessionImpl s "
    ),
	@NamedQuery(
            name = SessionImpl.QN.FIND_ALL_BY_START_DATE,
            query = "SELECT s FROM SessionImpl s " +
            		"WHERE s.startDate = :start_date"
    ),
	@NamedQuery(
            name = SessionImpl.QN.FIND_ALL_BY_END_DATE,
            query = "SELECT s FROM SessionImpl s " +
            		"WHERE s.endDate = :end_date"
    ),
	@NamedQuery(
            name = SessionImpl.QN.FIND_ALL_BY_LIMIT_BOOKING_DATE,
            query = "SELECT s FROM SessionImpl s " +
            		"WHERE s.limitBookingDate = :limit_booking_date"
    ),
	@NamedQuery(
            name = SessionImpl.QN.FIND_ALL_BY_PRICE,
            query = "SELECT s FROM SessionImpl s " +
            		"WHERE s.price = :price"
    ),
	@NamedQuery(
            name = SessionImpl.QN.FIND_ALL_BY_TOTAL_AVAILABLE_PLACE,
            query = "SELECT s FROM SessionImpl s " +
            		"WHERE s.totalAvailablePlace = :total_available_place"
    ),
    @NamedQuery(
            name = SessionImpl.QN.FIND_BY_EXTERNAL_ID,
            query = "SELECT s FROM SessionImpl s " +
                    "WHERE s.externalId = :external_id"
    ),
	 @NamedQuery(
	            name = SessionImpl.QN.FIND_ALL_BY_ADVENTURE_ID,
	            query = "SELECT s FROM SessionImpl s " +
	                    "WHERE s.adventure.externalId = :adventure_id"
	    )
})

public class SessionImpl implements Session{

	public static class QN {
		public static final String FIND_BY_EXTERNAL_ID = "SessionImpl.findByExtID";
		
		public static final String FIND_ALL = "SessionImpl.findAll";
		public static final String FIND_ALL_BY_START_DATE = "SessionImpl.findAllByStartDate";
		public static final String FIND_ALL_BY_END_DATE = "SessionImpl.findAllByEndDate";
		public static final String FIND_ALL_BY_LIMIT_BOOKING_DATE = "SessionImpl.findAllLimitBookingDate";
		public static final String FIND_ALL_BY_PRICE = "SessionImpl.findAllPrice";
		public static final String FIND_ALL_BY_TOTAL_AVAILABLE_PLACE = "SessionImpl.findAllTotalAvailablePlace";
		public static final String FIND_ALL_BY_ADVENTURE_ID = "SessionImpl.findAllByAdventureId";
	}
	
	@Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;
    
    @Column(name = DBUtils.START_DATE_DB)
    private Date startDate;
    
    @Column(name = DBUtils.END_DATE_DB)
    private Date endDate;
    
    @Column(name = DBUtils.LIMIT_BOOKING_DATE_DB)
    private Date limitBookingDate;
    
    @Column(name = DBUtils.PRICE_DB)
    private Double price;
    
    @Column(name = DBUtils.TOTAL_AVAILABLE_PLACE_DB)
    private Integer totalAvailablePlace;
    
    @Column(name = DBUtils.DESCRIPTION_DB)
    private String description;
    
    @ManyToOne(targetEntity = AdventureImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = DBUtils.ADVENTURE_ID_DB)
    private Adventure adventure;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@Override
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Override
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public Date getLimitBookingDate() {
		return limitBookingDate;
	}

	public void setLimitBookingDate(Date limitBookingDate) {
		this.limitBookingDate = limitBookingDate;
	}

	@Override
	public Double getPrice() {
		return price;
	}

	public Adventure getAdventure() {
		return adventure;
	}

	public void setAdventure(Adventure adventure) {
		this.adventure = adventure;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public Integer getTotalAvailablePlace() {
		return totalAvailablePlace;
	}

	public void setTotalAvailablePlace(Integer totalAvailablePlace) {
		this.totalAvailablePlace = totalAvailablePlace;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		return Objects.hash(externalId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SessionImpl))
			return false;
		SessionImpl other = (SessionImpl) obj;
		return Objects.equals(externalId, other.externalId);
	}
	
}
