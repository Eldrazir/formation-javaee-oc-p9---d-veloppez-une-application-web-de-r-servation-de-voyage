package oc.adventure.session.ms.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.jmapper.JMapper;

import oc.adventure.session.ms.controller.dto.SessionDto;
import oc.adventure.session.ms.entity.SessionImpl;
import oc.adventure.session.ms.entity.contract.Session;
import oc.adventure.session.ms.repository.contract.SessionRepository;
import oc.adventure.session.ms.service.contract.SessionService;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class SessionServiceImpl implements SessionService {	
	
    @Autowired
    private SessionRepository sessionRepository;
    
    @Autowired
    private JMapper<SessionDto, SessionImpl> sessionToDtoMapper;
    
    @Autowired
    private JMapper<SessionImpl, SessionDto> sessionDtoToSessionMapper;
    
    @Override
	public Session findByExternalId(String externalId){
        return this.sessionRepository.findByExternalId(externalId);
    }
    
    @Override
	public List<SessionDto> findAll(int pageNb, int nbPerPage){
    	List<Session> sessionList = this.sessionRepository.findAll(pageNb, nbPerPage);
    	return sessionList.stream().map(x -> sessionToDtoMapper.getDestination((SessionImpl) x)).collect(Collectors.toList());
    } 
    
    @Override
   	public List<SessionDto> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage){
       	List<Session> sessionList = this.sessionRepository.findAllByAdventureId(adventureId, pageNb, nbPerPage);
       	return sessionList.stream().map(x -> sessionToDtoMapper.getDestination((SessionImpl) x)).collect(Collectors.toList());
       }
      
    
}
