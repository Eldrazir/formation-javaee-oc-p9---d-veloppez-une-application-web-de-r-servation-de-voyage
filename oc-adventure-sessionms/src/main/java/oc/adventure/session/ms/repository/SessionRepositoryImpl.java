package oc.adventure.session.ms.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import oc.adventure.session.ms.entity.SessionImpl;
import oc.adventure.session.ms.entity.contract.Adventure;
import oc.adventure.session.ms.entity.contract.Session;
import oc.adventure.session.ms.repository.contract.SessionRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class SessionRepositoryImpl implements SessionRepository {
	
	@Autowired
    private EntityManager entityManager;
	
	@Override
    public Session findByExternalId(String externalId){
        TypedQuery<Session> sessionTypedQuery = this.entityManager.createNamedQuery(SessionImpl.QN.FIND_BY_EXTERNAL_ID, Session.class)
                .setParameter(DBUtils.EXTERNAL_ID_DB, externalId);

        return JPAUtils.getSingleResult(sessionTypedQuery);

    }
	
	public List<Session> findAllByStartDate(Date startDate, int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL_BY_START_DATE, Session.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}
	
	public List<Session> findAllByEndDate(Date endDate, int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL, Session.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}
	
	public List<Session> findAllByLimitBookingDate(int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL, Session.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}
	
	public List<Session> findAllByPrice(int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL, Session.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}
	
	public List<Session> findAllByStartDate(int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL, Session.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}
	
	@Override
	public List<Session> findAll(int pageNb, int nbPerPage){
		
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL, Session.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}	
	
	@Override
	public List<Session> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(SessionImpl.QN.FIND_ALL_BY_ADVENTURE_ID, Session.class).setParameter("adventure_id", adventureId)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}
	
	@Override
	public Session save(Session session) {		
		String externalId = JPAUtils.GetBase62(8);
        Session SessionFoundByExternal = this.findByExternalId(externalId);
        while(SessionFoundByExternal != null){
            externalId = JPAUtils.GetBase62(8);
            SessionFoundByExternal = this.findByExternalId(externalId);
        }
        this.entityManager.persist(session);
        return session;
		
	}
	
	@Override
	public void delete (Session session) {
		if(!this.entityManager.contains(session))
			this.entityManager.merge(session);
		
		this.entityManager.remove(session);
	}
	
	@Override
	public Session merge(Session session, Date startDate, Date endDate, Date limiteBookingDate, Double price,Integer totalAvailablePlace, String description, Adventure adventure) {
		
		if(!this.entityManager.contains(session))
			this.entityManager.merge(session);
		
		((SessionImpl) session).setStartDate(startDate);
		((SessionImpl) session).setEndDate(endDate);
		((SessionImpl) session).setLimitBookingDate(limiteBookingDate);
		((SessionImpl) session).setPrice(price);
		((SessionImpl) session).setTotalAvailablePlace(totalAvailablePlace);
		((SessionImpl) session).setDescription(description);
		((SessionImpl) session).setAdventure(adventure);
		
		return session;
		
	}

}
