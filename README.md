# Structure of the application

This is how the application is built :
* |
* |-docker-compose.yml
* |-oc-adventure-authorization-server/
* |-oc-adventure-data-consistency-manager/
* |-oc-adventure-discovery-service/
* |-oc-adventure-front/
* |-oc-adventure-gateway/
* |-oc-adventure-global/
* |-oc-adventure-shared-components/
* |-oc-adventure-utils/

At the end of this tutorial, you should have all of this component installed in your computer.

` First, you have to create an empty directory` for example "adventure-ms".

## Utility projects

Install the utility projects that are required for the micro services.

### oc-adventure-utils

The first one is *oc-adventure-utils/* which contains all the database utils component such as the columns name and utility method.

To install this :

`Go into your new folder, which is empty`

`git clone https://gitlab.com/Eldrazir/oc-adventure-utils.git`

### oc-adventure-shared-components

This second utility component is used to share components betwwen MicroService.
In this architecture, some components are shared to avoid code duplication.
This is one of the most usefull side components with `oc-adventure-data-consistency-manager`

To install this (stay in the same directory) :

`git clone https://gitlab.com/Eldrazir/oc-adventure-shared-components.git`

You must install these two components before running any Microservices mentioned below.

## Non-Business Microservices

There are some microservices which contains no business logic, but are required to launch the project.

### oc-adventure-gateway

This microservice is the external entry point for our application. All client requests will pass through this micro service.
For internal call between microservices, they will also call this gateway.

to install this :

`git clone https://gitlab.com/Mathprog1/oc-adventure-gateway.git`

### oc-adventure-discovery-service

This service is used to manager microservice call through scalability be registring all the instance of all the microservices to provide the right one to the gateway.

to install this :

`git clone https://gitlab.com/Mathprog1/oc-adventure-discovery-service.git`

## Business Microservices

Now let's talk about the business microservices. Each of them contains a part of the business logic of the application.

### oc-adventure-data-consistency-manager

This component is one of the most important. He is used to manage the integrity between the shared datas of all the microservices.
When an update is made, his role is to duplicate the update through all the microservices.

to install this :

`git clone https://gitlab.com/Mathprog1/oc-adventure-data-consistency-manager.git`

### oc-adventure-authorization-server

This component is used to generate the Java Web Token (jwt). It is the only component which is able to generate
a token. ALl the Microservices are able to validate the signature of this token, once he has been generated.

to install this :

`git clone https://gitlab.com/Mathprog1/oc-adventure-authorization-server.git`

### oc-adventure-global

This is the core component of the application. It contains all the core business microservice such as user component, address component, etc.
It contains all the independant microservices wich are grouped into this repository.

To install this :

`git clone https://gitlab.com/Eldrazir/formation-javaee-oc-p9---d-veloppez-une-application-web-de-r-servation-de-voyage.git`

# Launch the application

There are only few steps required to launch the application.

## 1. Build the projects

First, go into the _oc-adventure-global_ which containes the parent pom required to easyly package all the components.

`cd oc-adventure-global` (or formation-javaee-oc-p9---d-veloppez-une-application-web-de-r-servation-de-voyage)

Then, run the maven install command line :

`mvn clean install`

This command will generate all the fat jar required to Docker.

## Launch all the microservices with docker-compose

If you paye attention, there is a Docker file in each projet. THis docker file is used to build the docker image required for docker-compose deployment.

Go back in your parent directory : `cd ..`

Copy the docker-compose file : `cp oc-adventure-global/docker-compose.yml` (or formation-javaee-oc-p9---d-veloppez-une-application-web-de-r-servation-de-voyage/docker-compose.yml)

Open your docker-compose terminal, go to your parent directory then run :

`docker-compose build` 

Now docker-compose has build your images. You juste have to launch them :

`docker-compose up -d`

Now your microservices has been launched. You can go on the URL :

http://192.168.99.100:8080/eureka/web if you are on Windows / Macos
http://localhost:8080/eureka/web if you are on Linux.

## Launch the Front app

To run the Front App in local, you'll need to install NodeJs and Angular-cli.
Tutorial is linked here : https://www.javatpoint.com/angular-8-installation

To install this :

`git clone https://gitlab.com/Mathprog1/oc-adventure-front.git`

Then to launch the app :

`cd oc-adventure-front`

`ng serve`

The application is now running on http://localhost:4200/

# Documentation

The document has been generated with swagger-ui 2.0. It is avaible for each microservices.

You can see it on :

`http://192.168.99.100:8080/{{microservice}}/v2/api-docs` if you are on Windows / Macos

`http://localhost:8080/{{microservice}}/v2/api-docs` if you are on Linux.

