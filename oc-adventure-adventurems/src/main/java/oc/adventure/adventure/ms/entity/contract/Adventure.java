package oc.adventure.adventure.ms.entity.contract;

import java.util.*;

import oc.adventure.shared.components.entity.contract.AdventureParent;


public interface Adventure extends AdventureParent{
	void addCategory(Category category);

	void removeCategory(Category category);
	
	int calculateRating(int totalVoters, int totalVoting);
	

	String getLocationAdventure();

	String getDescriptionAdventure();

	int getTotalVoters();

	int getTotalVoting();
	
	int getAdventureRating();
	
	Set<Category> getCategoryList();
	
	
}
