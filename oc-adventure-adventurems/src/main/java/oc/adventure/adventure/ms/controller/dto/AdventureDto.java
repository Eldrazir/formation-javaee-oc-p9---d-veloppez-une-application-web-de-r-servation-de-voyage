package oc.adventure.adventure.ms.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.googlecode.jmapper.annotations.JMap;

import oc.adventure.adventure.ms.entity.contract.Category;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class AdventureDto implements Serializable {

    @JMap // Pour indiquer qu'il doit automatiquement Mapper cet attribut avec l'attribut de même nom de l'entité Adventure (hibernate)
    private String externalId;

    @JMap
    private String nameAdventure;

    @JMap
    private String locationAdventure;

    @JMap
    private String descriptionAdventure;

    @JMap
    private int totalVoters;
    
    @JMap
    private int totalVoting;
    
    @JMap
    private int adventureRating;

    @JMap
  	List<Category> categoryList;
    
	public AdventureDto() {
		
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getNameAdventure() {
		return nameAdventure;
	}

	public void setNameAdventure(String nameAdventure) {
		this.nameAdventure = nameAdventure;
	}

	public String getLocationAdventure() {
		return locationAdventure;
	}

	public void setLocationAdventure(String locationAdventure) {
		this.locationAdventure = locationAdventure;
	}

	public String getDescriptionAdventure() {
		return descriptionAdventure;
	}

	public void setDescriptionAdventure(String descriptionAdventure) {
		this.descriptionAdventure = descriptionAdventure;
	}

	public int getTotalVoters() {
		return totalVoters;
	}

	public void setTotalVoters(int totalVoters) {
		this.totalVoters = totalVoters;
	}

	public int getTotalVoting() {
		return totalVoting;
	}

	public void setTotalVoting(int totalVoting) {
		this.totalVoting = totalVoting;
	}

	public int getAdventureRating() {
		return adventureRating;
	}

	public void setAdventureRating(int adventureRating) {
		this.adventureRating = adventureRating;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}


	
	
    
    
   
}
