package oc.adventure.adventure.ms.controller.config;

import com.googlecode.jmapper.JMapper;


import oc.adventure.adventure.ms.controller.dto.AdventureDto;
import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.entity.AdventureImpl;
import oc.adventure.adventure.ms.entity.CategoryImpl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DtoJMapperInit {

     @Bean
    JMapper<AdventureDto, AdventureImpl> adventureMapper(){
        return new JMapper<>(AdventureDto.class, AdventureImpl.class);
    }
     
     @Bean
     JMapper<CategoryDto, CategoryImpl> categoryMapper(){
         return new JMapper<>(CategoryDto.class, CategoryImpl.class);
     }

     @Bean
     JMapper<AdventureImpl,AdventureDto> adventureToMapper(){
         return new JMapper<>( AdventureImpl.class, AdventureDto.class);
     }
      
      @Bean
      JMapper<CategoryImpl,CategoryDto> categoryToMapper(){
          return new JMapper<>(CategoryImpl.class,CategoryDto.class);
      }
}


