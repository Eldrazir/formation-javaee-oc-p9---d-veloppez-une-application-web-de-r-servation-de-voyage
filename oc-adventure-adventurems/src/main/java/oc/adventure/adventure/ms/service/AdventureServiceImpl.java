package oc.adventure.adventure.ms.service;


import com.googlecode.jmapper.JMapper;

import oc.adventure.adventure.ms.controller.dto.AdventureDto;
import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.entity.AdventureImpl;
import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;

import oc.adventure.adventure.ms.repository.contract.AdventureRepository;
import oc.adventure.adventure.ms.repository.contract.CategoryRepository;
import oc.adventure.adventure.ms.service.contract.AdventureService;
import oc.adventure.utils.DBUtils;

import oc.adventure.utils.DBUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class AdventureServiceImpl implements AdventureService {


    @Autowired
    private AdventureRepository adventureRepository;

    @Autowired
    private CategoryRepository categoryRepository;
 

    @Autowired
    private JMapper<AdventureDto, AdventureImpl> adventureMapper;

    @Autowired
    private JMapper<AdventureImpl, AdventureDto> adventureDtoToMapper;



    @Override
    public AdventureDto register(AdventureDto adventureDto){

        Adventure adventure =  this.adventureRepository.save(adventureDtoToMapper.getDestination(adventureDto));
        Category categoryAdventure = this.categoryRepository.findByTypeAdventure(DBUtils.TYPE_ADVENTURE_DB);
        adventure.addCategory(categoryAdventure);
        categoryAdventure.addAdventure(adventure);
        return adventureMapper.getDestination((AdventureImpl) adventure);

    }
    @Override
    public AdventureDto findByExternalId(String externalId){
        return adventureMapper.getDestination((AdventureImpl) this.adventureRepository.findByExtId(externalId));
    }


    @Override
    public List<AdventureDto> findAll(int pageNb, int nbPerPage) {
        List<Adventure> adventureList = this.adventureRepository.findAll(pageNb, nbPerPage);
        return adventureList.stream().map(x -> adventureMapper.getDestination((AdventureImpl) x)).collect(Collectors.toList());

    }



    @Override
    public AdventureDto findByLocationAdventure(String locationAdventure) {

        return adventureMapper.getDestination((AdventureImpl) this.adventureRepository.findByLocationAdventure(locationAdventure));
    }

    @Override
    public AdventureDto findByAdventureRating(int adventureRating) {
        return adventureMapper.getDestination((AdventureImpl) this.adventureRepository.findByAdventureRating(adventureRating));
    }

    @Override
    public AdventureDto findByNameAdventure(String nameAdventure) {
        return adventureMapper.getDestination((AdventureImpl) this.adventureRepository.findByNameAdventure(nameAdventure));
    }

    @Override
    public int calculateRating(int totalVoters, int totalVoting) {
//TO DO
        return 0;
    }

    @Override
    public AdventureDto addAdventure(AdventureDto adventureDto) {
        Adventure adventure =  this.adventureRepository.save(adventureDtoToMapper.getDestination(adventureDto));
        Category categoryAdventure = this.categoryRepository.findByTypeAdventure(DBUtils.CATEGORY);
        adventure.addCategory(categoryAdventure);
        categoryAdventure.addAdventure(adventure);
        return adventureMapper.getDestination((AdventureImpl) adventure);

    }


    @Override
    public AdventureDto update(AdventureDto adventureDto) {
        Adventure adventure = this.adventureRepository.findByExtId(adventureDto.getExternalId());
        if(adventure == null)
            return null;
        return adventureMapper.getDestination((AdventureImpl) this.adventureRepository.merge(adventure, adventureDto.getDescriptionAdventure(),adventureDto.getLocationAdventure(),adventureDto.getNameAdventure()));


    }

    @Override
    public boolean delete(String externalId) {
        Adventure adventure = this.adventureRepository.findByExtId(externalId);
        if(adventure == null)
            return false;
        this.adventureRepository.delete(adventure);
        adventure = this.adventureRepository.findByExtId(externalId);
        return (adventure == null);

    }
    @Override
    public List<AdventureDto> findAllByCategory(String typeAdventure) {

		  Set<Adventure> adventureList = this.categoryRepository.findByTypeAdventure(typeAdventure).getAdventureList();
	        return adventureList.stream().map(x -> adventureMapper.getDestination((AdventureImpl) x)).collect(Collectors.toList());
                 
         }

    
}

