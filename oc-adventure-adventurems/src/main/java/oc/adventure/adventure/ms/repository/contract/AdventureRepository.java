package oc.adventure.adventure.ms.repository.contract;

import java.util.List;


import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.shared.components.repository.AdventureRepositoryParent;

public interface AdventureRepository <T extends Adventure> extends AdventureRepositoryParent<T>{
    T find(int id);

    T findByLocationAdventure(String locationAdventure);

    T findByExtId(String extId);

    T findByNameAdventure(String nameAdventure);

    T findByAdventureRating(int adventureRating);
    
   
    List<T> findAll(int pageNb, int nbPerPage);
    Adventure save(T adventure);

    void delete(T adventure);

    Adventure merge(T adventure, String nameAdventure, String locationAdventure, String descriptionAdventure);

    //add and remove category
    
  
}
