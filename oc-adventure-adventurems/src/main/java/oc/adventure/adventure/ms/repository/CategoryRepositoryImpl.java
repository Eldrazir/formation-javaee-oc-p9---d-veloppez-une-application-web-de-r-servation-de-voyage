package oc.adventure.adventure.ms.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import oc.adventure.adventure.ms.entity.AdventureImpl;
import oc.adventure.adventure.ms.entity.CategoryImpl;
import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.adventure.ms.repository.contract.CategoryRepository;


import oc.adventure.utils.DBUtils;

import oc.adventure.utils.JPAUtils;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CategoryRepositoryImpl implements CategoryRepository {



    @Autowired
    private EntityManager entityManager;

    @Override
    public Category find(int id) {
        return this.entityManager.find(Category.class, id);
    }


    @Override
    public Category findByTypeAdventure(String typeAdventure) {
        TypedQuery<Category> CategoryTypedQuery = entityManager.createNamedQuery(CategoryImpl.QN.FIND_BY_TYPE_ADVENTURE, Category.class).setParameter("type_adventure", typeAdventure);

        return JPAUtils.getSingleResult(CategoryTypedQuery);
    }

    @Override
    public Category findByExternalId(String extId){
        TypedQuery<Category> categoryTypedQuery = this.entityManager.createNamedQuery(CategoryImpl.QN.FIND_BY_EXT_ID, Category.class)
                .setParameter("ext_id", extId);

        return JPAUtils.getSingleResult(categoryTypedQuery);

    }

    @Override
    public List<Category> findAll(int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(CategoryImpl.QN.FIND_ALL, Category.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }




    @Override
    public Category save(Category category) {
        Category CategoryFound = this.findByTypeAdventure(category.getTypeAdventure());
        if(CategoryFound != null)
            return null;

        String externalId = JPAUtils.GetBase62(8);


        Category CategoryFoundByExternal = this.findByExternalId(externalId);
        while(CategoryFoundByExternal != null){
            externalId = JPAUtils.GetBase62(8);
            CategoryFoundByExternal = this.findByExternalId(externalId);

        }
        ((CategoryImpl) category).setExternalId(externalId);
        this.entityManager.persist(category);
        return category;
    }

    @Override
    public void delete(Category category) {
        if(!this.entityManager.contains(category))
            this.entityManager.merge(category);

        this.entityManager.remove(category);
    }

    @Override
    public Category merge(Category category, String typeAdventure) {


        Category CategoryFound = this.findByTypeAdventure(typeAdventure);
        if(CategoryFound != null && !category.equals(CategoryFound))
            return null;

        if(!this.entityManager.contains(category))
            this.entityManager.merge(category);

        ((CategoryImpl) category).setTypeAdventure(typeAdventure);


        return category;
    }



}
