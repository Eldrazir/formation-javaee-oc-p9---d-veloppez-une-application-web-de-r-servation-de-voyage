package oc.adventure.adventure.ms.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.utils.DBUtils;



@Entity
@Table(name = DBUtils.CATEGORY)
@NamedQueries({

		@NamedQuery(
				name = CategoryImpl.QN.FIND_ALL,
				query = "SELECT c FROM CategoryImpl c "
		),


		@NamedQuery(
				name = CategoryImpl.QN.FIND_BY_EXT_ID,
				query = CategoryImpl.QN.SELECT_FROM +
						"WHERE c.externalId = :ext_id"
		),
		@NamedQuery(
				name = CategoryImpl.QN.FIND_BY_TYPE_ADVENTURE,
				query = CategoryImpl.QN.SELECT_FROM +
						"WHERE c.typeAdventure = :type_adventure"
		),


})
public class CategoryImpl implements Category {

	public static class QN {
		public static final String FIND_BY_TYPE_ADVENTURE = "CategoryImpl.findByTypeAdventure";

		//public static final String FIND_BY_EXTERN_ID = "CategoryImpl.findByExtID";

		public static final String FIND_BY_EXT_ID = "CategoryImpl.findByExtID";

		public static final String FIND_ALL = "CategoryImpl.findAll";
		public static final String SELECT_FROM = "SELECT c FROM CategoryImpl c ";

	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
	private String externalId;


	@Column(name = DBUtils.TYPE_ADVENTURE_DB)
	private String typeAdventure;


	//@ManyToMany(targetEntity = CategoryImpl.class, fetch = FetchType.LAZY)
	/*@JoinTable(
			name = "category_adventure",
			joinColumns = @JoinColumn(
					name = "category_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(
					name = "adventure_id", referencedColumnName = "id"))
					*/
	@JsonBackReference
	@ManyToMany(targetEntity = AdventureImpl.class, mappedBy = "categoryList", fetch=FetchType.LAZY)
	
	private Set<Adventure> adventureList = new HashSet<Adventure>();

	public CategoryImpl() {
	}



	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}



	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof AdventureImpl)) return false;
		AdventureImpl adventure = (AdventureImpl) o;
		return getExternalId().equals(adventure.getExternalId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getExternalId());
	}



	@Override
	public void addAdventure(Adventure adventure) {
		adventure.getCategoryList().add(this);
		this.adventureList.remove(adventure);

	}


	@Override
	public void removeAdventure(Adventure adventure) {
		adventure.getCategoryList().remove(this);
		this.adventureList.remove(adventure);

	}



	@Override
	public Integer getCategoryId() {

		return id;
	}



	@Override
	public String getTypeAdventure() {

		return typeAdventure;
	}



	@Override
	public Set<Adventure> getAdventureList() {

		return adventureList;
	}

	public void setAdventureList(Set<Adventure> adventureList) {
		this.adventureList = adventureList;
	}



	public void setTypeAdventure(String typeAdventure) {
		this.typeAdventure = typeAdventure;
	}


}
