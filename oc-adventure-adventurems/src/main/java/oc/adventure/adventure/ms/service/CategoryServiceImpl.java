package oc.adventure.adventure.ms.service;

import com.googlecode.jmapper.JMapper;


import oc.adventure.adventure.ms.controller.dto.AdventureDto;
import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.entity.AdventureImpl;
import oc.adventure.adventure.ms.entity.CategoryImpl;
import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.adventure.ms.repository.contract.AdventureRepository;
import oc.adventure.adventure.ms.repository.contract.CategoryRepository;
import oc.adventure.adventure.ms.service.contract.CategoryService;
import oc.adventure.utils.DBUtils;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(propagation = Propagation.MANDATORY)
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private AdventureRepository adventureRepository;

	@Autowired
	private CategoryRepository categoryRepository;


	@Autowired
	JMapper<CategoryImpl,CategoryDto> categoryToDtoMapper;

	@Autowired
	JMapper<CategoryDto, CategoryImpl> categoryToMapper;

	@Override
	public List<CategoryDto> findAll(int pageNb, int nbPerPage) {
		List<Category> categoryList = this.categoryRepository.findAll(pageNb, nbPerPage);
		return categoryList.stream().map(x -> categoryToMapper.getDestination((CategoryImpl) x)).collect(Collectors.toList());

	}

	@Override
	public CategoryDto findByExternalId(String externalId) {
		return categoryToMapper.getDestination((CategoryImpl) this.categoryRepository.findByExternalId(externalId));
	}

	@Override
	public CategoryDto findByTypeAdventure(String typeAdventure) {
		return categoryToMapper.getDestination((CategoryImpl) this.categoryRepository.findByTypeAdventure(typeAdventure));
	}

	@Override
	public CategoryDto addCategory(CategoryDto categoryDto) {
		Category category =  this.categoryRepository.save(categoryToDtoMapper.getDestination(categoryDto));
		Adventure adventureCategory = this.adventureRepository.findByNameAdventure(DBUtils.NAME_ADVENTURE_DB);

		category.addAdventure(adventureCategory);

		return categoryToMapper.getDestination((CategoryImpl) category);

	}

	@Override
	public CategoryDto update(CategoryDto categoryDto) {
		Category category =  this.categoryRepository.findByExternalId(categoryDto.getExternalId());
		if(category == null)
			return null;
		return categoryToMapper.getDestination((CategoryImpl) this.categoryRepository.merge(category, categoryDto.getTypeAdventure()));


	}

	@Override
	public boolean delete(String externalId) {
		Category category =  this.categoryRepository.findByExternalId(externalId);
		if(category == null)
			return false;
		this.categoryRepository.delete(category);
		category = this.categoryRepository.findByExternalId(externalId);
		return (category == null);
	}

	@Override
	public Category findCategoryByTypeAdventure(String typeAdventure) {
		
		
		Category category = this.categoryRepository.findByTypeAdventure(typeAdventure);
		
			
		return category;
		
		
		
	}


}
