package oc.adventure.adventure.ms.service.contract;

import java.util.List;

import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.entity.contract.Category;


public interface CategoryService {

	
	List<CategoryDto> findAll(int pageNb, int nbPerPage);
	
	CategoryDto findByExternalId(String externalId);
	
	CategoryDto addCategory(CategoryDto categoryDto);

	CategoryDto update(CategoryDto categoryDto);

	    boolean delete(String externalId);
	    
	    CategoryDto findByTypeAdventure(String typeAdventure);
	     Category findCategoryByTypeAdventure(String typeAdventure);
}
