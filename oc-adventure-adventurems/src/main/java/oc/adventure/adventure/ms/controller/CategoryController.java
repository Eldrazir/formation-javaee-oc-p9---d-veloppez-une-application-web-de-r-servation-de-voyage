package oc.adventure.adventure.ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import oc.adventure.adventure.ms.controller.dto.AdventureDto;
import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.service.contract.AdventureService;
import oc.adventure.adventure.ms.service.contract.CategoryService;



@RestController
@RequestMapping("/api/v1/ms/category")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CategoryController {

	
	
	 @Autowired
	    private CategoryService categoryService;

	   @GetMapping(value    = "/",
	            produces = "application/json")
	    public List<CategoryDto> getAll(@RequestParam int pageNg, @RequestParam int nbPerPage){
	        return this.categoryService.findAll(pageNg, nbPerPage);
	    }
	   
	   
	   
	   @GetMapping(value    = "/{externalId}",
	            produces = "application/json")
	    @ResponseBody
	    public CategoryDto getByExternalId(@PathVariable("externalId") String externalId){
	        return this.categoryService.findByExternalId(externalId);
	    }
	   
	   
}
