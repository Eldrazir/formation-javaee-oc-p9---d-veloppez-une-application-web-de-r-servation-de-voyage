package oc.adventure.adventure.ms.controller;


import com.googlecode.jmapper.JMapper;

import oc.adventure.adventure.ms.controller.dto.AdventureDto;
import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.entity.AdventureImpl;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.adventure.ms.service.contract.AdventureService;
import oc.adventure.adventure.ms.service.contract.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ms/adventure")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class AdventureController {

    @Autowired
    private AdventureService adventureService;
    
    @Autowired
    private CategoryService categoryService;

    @GetMapping(value    = "/",
            produces = "application/json")
    @ResponseBody
    public List<AdventureDto> getAll(@RequestParam int pageNg, @RequestParam int nbPerPage){
        return this.adventureService.findAll(pageNg, nbPerPage);
    }
    
    

    @GetMapping(value    = "/all",
            produces = "application/json")
    @ResponseBody
    public List<AdventureDto> getAll(){
    	int pageNg=1;
    	int nbPerPage=1;
        return this.adventureService.findAll(pageNg, nbPerPage);
    }

    @GetMapping(value    = "/{externalId}",
            produces = "application/json")
    @ResponseBody
    public AdventureDto getByExternalId(@PathVariable("externalId") String externalId){
        return this.adventureService.findByExternalId(externalId);
    }

    
    @GetMapping(value    = "/adventurename/{nameAdventure}",
            produces = "application/json")
    @ResponseBody
    public AdventureDto getByNameAdventure(@PathVariable("nameAdventure") String nameAdventure){
        return this.adventureService.findByNameAdventure(nameAdventure);
    }
    
    @GetMapping(value    = "/adventurelocation/{locationAdventure}",
            produces = "application/json")
    @ResponseBody
    public AdventureDto getByLocationAdventure(@PathVariable("locationAdventure") String locationAdventure){
        return this.adventureService.findByLocationAdventure(locationAdventure);
    }
    
    @GetMapping(value    = "/rating/{adventureRating}",
            produces = "application/json")
    @ResponseBody
    public AdventureDto getByAdventureRating(@PathVariable("adventureRating") String adventureRating){
        return this.adventureService.findByLocationAdventure(adventureRating);
    }
    
    //TODO A VERIFIER
    @GetMapping(value    = "/adventure-category/{typeAdventure}",
            produces = "application/json")
    @ResponseBody
    public List<AdventureDto> getByCategory(@PathVariable("typeAdventure") String typeAdventure){
    	   	
       	
    	return this.adventureService.findAllByCategory(typeAdventure);
    }

    @PostMapping(value    = "register/",
            produces = "application/json",
            consumes = "application/json")
    public AdventureDto register (@RequestBody AdventureDto adventureDto){
        return this.adventureService.register(adventureDto);
    }
    
    
    @DeleteMapping(value    = "delete/{externalId}",
            produces = "application/json")
    @ResponseBody
    public boolean delete(@PathVariable("externalId") String externalId){
        return this.adventureService.delete(externalId);
    }


    @PutMapping(value    = "update/",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public AdventureDto update(@RequestBody AdventureDto adventureDto){
        return this.adventureService.update(adventureDto);
    }

}
