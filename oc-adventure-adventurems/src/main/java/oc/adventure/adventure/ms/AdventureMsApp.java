package oc.adventure.adventure.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages = "oc.adventure.adventure.ms")
@EnableSwagger2
public class AdventureMsApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(AdventureMsApp.class, args);
    }
}
