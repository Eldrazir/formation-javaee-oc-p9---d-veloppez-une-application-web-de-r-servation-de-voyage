package oc.adventure.adventure.ms.controller.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.googlecode.jmapper.annotations.JMap;

import oc.adventure.adventure.ms.entity.contract.Adventure;

public class CategoryDto implements Serializable{
	
	@JMap
	private String externalId;
	
	@JMap
    private String typeAdventure;

	@JMap
	private List<Adventure> adventureList;
	 
	
	public CategoryDto() {
		
	}
	
	

	public String getExternalId() {
		return externalId;
	}



	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}



	public String getTypeAdventure() {
		return typeAdventure;
	}

	public void setTypeAdventure(String typeAdventure) {
		this.typeAdventure = typeAdventure;
	}



	public List<Adventure> getAdventureList() {
		return adventureList;
	}



	public void setAdventureList(List<Adventure> adventureList) {
		this.adventureList = adventureList;
	}



	


}
