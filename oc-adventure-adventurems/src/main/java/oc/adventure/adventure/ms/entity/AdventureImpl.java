package oc.adventure.adventure.ms.entity;

import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.shared.components.entity.AdventureParentImpl;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.HashSet;

import java.util.Objects;
import java.util.Set;


@Entity
@Table(name = DBUtils.ADVENTURE)
@NamedQueries({
	

     
        @NamedQuery(
                name = AdventureImpl.QN.FIND_BY_ADVENTURE_RATING,
                query = AdventureImpl.QN.SELECT_FROM +
                        "WHERE a.adventureRating = :adventure_rating"
        ),
        @NamedQuery(
                name = AdventureImpl.QN.FIND_BY_LOCATION_ADVENTURE,
                query = AdventureImpl.QN.SELECT_FROM +
                "WHERE a.locationAdventure = :location_adventure"
                ),

       
})
public class AdventureImpl extends AdventureParentImpl implements Adventure {

    public static class QN {
        public static final String FIND_BY_LOCATION_ADVENTURE = "AdventureImpl.findByLocationAdventure";
        public static final String FIND_BY_EXT_ID = "AdventureImpl.findByExtID";
        public static final String FIND_BY_NAME_ADVENTURE = "AdventureImpl.findByNameAdventure";
        public static final String FIND_BY_ADVENTURE_RATING = "AdventureImpl.findByAdventureRating";

        public static final String FIND_ALL = "AdventureImpl.findAll";
   
        public static final String SELECT_FROM = "SELECT a FROM AdventureImpl a ";
    
    }

  
    @Column(name = DBUtils.LOCATION_ADVENTURE_DB)
    private String locationAdventure;
    
    @Column(name = DBUtils.DESCRIPTION_ADVENTURE_DB, length = 3000)
    private String descriptionAdventure;

    @Column(name = DBUtils.TOTAL_VOTERS_DB)
    private int totalVoters;

    @Column(name = DBUtils.TOTAL_VOTING_DB)
    private int totalVoting;
    
    @Column(name = DBUtils.ADVENTURE_RATING_DB)
    private int adventureRating;


   
   /*@JoinTable(
            name = "adventure_category",
            joinColumns = @JoinColumn(
                    name = "adventure_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "category_id", referencedColumnName = "id"))    */
    @JsonBackReference
    @ManyToMany(targetEntity = CategoryImpl.class, fetch = FetchType.LAZY)
    @JoinTable(name = "adventure_category",
    joinColumns = {
            @JoinColumn(name = "adventure_id", referencedColumnName = "id")},
    inverseJoinColumns = {
            @JoinColumn(name = "category_id", referencedColumnName = "id")})
    private Set<Category> categoryList = new HashSet<Category>();

    public AdventureImpl() {
    }

  
  

	@Override
	public String getLocationAdventure() {
		return locationAdventure;
	}

	@Override
	public String getDescriptionAdventure() {
		return descriptionAdventure;
	}

	@Override
	public int getTotalVoters() {
		
		return totalVoters;
	}

	@Override
	public int getTotalVoting() {
		return totalVoting;
	}
	  @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof AdventureImpl)) return false;
	        AdventureImpl adventure = (AdventureImpl) o;
	        return getExternalId().equals(adventure.getExternalId());
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(getExternalId());
	    }

		@Override
		public void addCategory(Category category) {
		category.getAdventureList().add(this);
		this.categoryList.add(category);
			
		}


		@Override
		public void removeCategory(Category category) {
			category.getAdventureList().remove(this);
			this.categoryList.remove(category);
			
		}


		@Override
		public int calculateRating(int totalVoters, int totalVoting) {
			adventureRating = totalVoting/totalVoters;
			return adventureRating;
		}


		@Override
		public Set<Category> getCategoryList() {
			
			return categoryList;
		}
		
	    public void setCategoryList(Set<Category> categoryList) {
	        this.categoryList = categoryList;
	    }


		@Override
		public int getAdventureRating() {
			return adventureRating;
		}


	

		public void setLocationAdventure(String locationAdventure) {
			this.locationAdventure = locationAdventure;
		}


		public void setDescriptionAdventure(String descriptionAdventure) {
			this.descriptionAdventure = descriptionAdventure;
		}


		public void setTotalVoters(int totalVoters) {
			this.totalVoters = totalVoters;
		}


		public void setTotalVoting(int totalVoting) {
			this.totalVoting = totalVoting;
		}


		public void setAdventureRating(int adventureRating) {
			this.adventureRating = adventureRating;
		}



}
