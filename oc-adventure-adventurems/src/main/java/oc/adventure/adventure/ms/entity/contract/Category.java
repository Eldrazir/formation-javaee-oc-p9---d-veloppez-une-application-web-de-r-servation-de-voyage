package oc.adventure.adventure.ms.entity.contract;

import java.util.List;
import java.util.Set;

public interface Category {
	void addAdventure(Adventure adventure);

	void removeAdventure(Adventure adventure);
	
	Integer getCategoryId();

	String getExternalId();

	String getTypeAdventure();

	Set<Adventure> getAdventureList();
}
