package oc.adventure.adventure.ms.repository.contract;


import java.util.List;

import oc.adventure.adventure.ms.entity.contract.Category;

public interface CategoryRepository {


	Category findByExternalId(String externalId);

	Category findByTypeAdventure(String typeAdventure);



	List<Category> findAll(int pageNb, int nbPerPage);

	Category save(Category category);

	void delete(Category category);

	Category merge(Category category, String typeAdventure);

	Category find(int id);




}
