package oc.adventure.adventure.ms.repository;

import oc.adventure.adventure.ms.entity.AdventureImpl;

import oc.adventure.adventure.ms.entity.contract.Adventure;
import oc.adventure.adventure.ms.entity.contract.Category;
import oc.adventure.adventure.ms.repository.contract.AdventureRepository;
import oc.adventure.shared.components.repository.AdventureRepositoryParentImpl;
import oc.adventure.utils.JPAUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AdventureRepositoryImpl extends AdventureRepositoryParentImpl<Adventure >implements AdventureRepository<Adventure> {



	@Override
	public Adventure findByLocationAdventure(String locationAdventure) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_LOCATION_ADVENTURE, Adventure.class)
				.setParameter("location_adventure", locationAdventure);

		return JPAUtils.getSingleResult(AdventureTypedQuery);
	}

	@Override
	public Adventure findByExtId(String extId) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_EXT_ID, Adventure.class).setParameter("ext_id", extId);
		return JPAUtils.getSingleResult(AdventureTypedQuery);
	}

	@Override
	public Adventure findByNameAdventure(String nameAdventure) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_NAME_ADVENTURE, Adventure.class)
				.setParameter("name_adventure", nameAdventure);

		return JPAUtils.getSingleResult(AdventureTypedQuery);

	}

	@Override
	public Adventure findByAdventureRating(int adventureRating) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_ADVENTURE_RATING, Adventure.class)
				.setParameter("adventure_rating", adventureRating);

		return JPAUtils.getSingleResult(AdventureTypedQuery);
	}

	@Override
	public Adventure save(Adventure adventure) {
		Adventure AdventureFound = this.findByNameAdventure(adventure.getNameAdventure());
		if (AdventureFound != null)
			return null;

		String externalId = JPAUtils.GetBase62(8);
		Adventure AdventureFoundByExternal = this.findByExtId(externalId);
		while (AdventureFoundByExternal != null) {
			externalId = JPAUtils.GetBase62(8);
			AdventureFoundByExternal = this.findByExtId(externalId);
		}
		((AdventureImpl) adventure).setExternalId(externalId);
		this.entityManager.persist(adventure);
		return adventure;
	}

	@Override
	public void delete(Adventure adventure) {
		if (!this.entityManager.contains(adventure))
			this.entityManager.merge(adventure);

		this.entityManager.remove(adventure);

	}

	@Override
	public Adventure merge(Adventure adventure, String nameAdventure, String locationAdventure,
			String descriptionAdventure) {
		Adventure AdventureFound = this.findByNameAdventure(nameAdventure);
		if (AdventureFound != null && !adventure.equals(AdventureFound))
			return null;

		if (!this.entityManager.contains(adventure))
			this.entityManager.merge(adventure);

		((AdventureImpl) adventure).setLocationAdventure(locationAdventure);
		((AdventureImpl) adventure).setDescriptionAdventure(descriptionAdventure);

		return adventure;
	}

	@Override
	public List<Adventure> findAll(int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(AdventureImpl.QN.FIND_ALL, Adventure.class)
				.setFirstResult((pageNb - 1) * pageNb).setMaxResults(nbPerPage).getResultList();
	}

	@Override
	public Class<Adventure> getEntityTClass() {
		return Adventure.class;
	}


	
	
}
