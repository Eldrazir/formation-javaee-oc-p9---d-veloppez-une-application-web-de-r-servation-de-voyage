package oc.adventure.adventure.ms.service.contract;

import java.util.List;

import oc.adventure.adventure.ms.controller.dto.AdventureDto;
import oc.adventure.adventure.ms.controller.dto.CategoryDto;
import oc.adventure.adventure.ms.entity.contract.Category;

public interface AdventureService {
    List<AdventureDto> findAll(int pageNb, int nbPerPage);

    AdventureDto register(AdventureDto adventureDto);
    
     AdventureDto findByExternalId(String externalId);
    
    AdventureDto findByLocationAdventure(String locationAdventure);

    AdventureDto findByAdventureRating(int adventureRating);
    
    AdventureDto findByNameAdventure(String nameAdventure);

    int calculateRating(int totalVoters, int totalVoting);

    AdventureDto addAdventure(AdventureDto adventureDto);

    AdventureDto update(AdventureDto adventureDto);

    boolean delete(String externalId);

    List<AdventureDto> findAllByCategory(String typeAdventure);

}
