package oc.adventure.address.ms.controller;


import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressInDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;
import oc.adventure.address.ms.service.contract.TypeAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ms/address/typeaddress")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class TypeAddressController {

    @Autowired
    private TypeAddressService typeAddressService;

    @GetMapping(value    = "/",
            produces = "application/json")
    @ResponseBody
    public List<TypeAddressOutDto> getAll(@RequestParam int pageNb, @RequestParam int nbPerPage){
        return this.typeAddressService.findAll(pageNb, nbPerPage);
    }

    @GetMapping(value    = "/name/like/{nameLike}",
            produces = "application/json")
    @ResponseBody
    public List<TypeAddressOutDto> getAllByNameLike(@PathVariable(name = "nameLike") String nameLike, @RequestParam int pageNb, @RequestParam int nbPerPage){
        return this.typeAddressService.findAllByNameLike(nameLike, pageNb, nbPerPage);
    }

    @GetMapping(value    = "/name/{name}",
            produces = "application/json")
    @ResponseBody
    public TypeAddressOutDto getByName(@PathVariable(name = "name") String name){
        return this.typeAddressService.findByName(name);
    }

    @PostMapping(path = "/create/",
        produces = "application/json",
        consumes = "application/json")
    public TypeAddressOutDto create(@RequestBody TypeAddressInDto typeAddressInDto){
        return this.typeAddressService.create(typeAddressInDto);
    }

    @PutMapping(path = "/update/",
        produces = "application/json",
        consumes = "application/json")
    public TypeAddressOutDto update(@RequestBody TypeAddressOutDto typeAddressOutDto){
        return this.typeAddressService.merge(typeAddressOutDto);
    }

    @DeleteMapping(path = "/delete/{externalId}",
        produces = "application/json",
        consumes = "application/json")
    public boolean delete(@PathVariable(name = "externalId") String externalId){
        return this.typeAddressService.delete(externalId);
    }

}
