package oc.adventure.address.ms.repository.contract;

import oc.adventure.address.ms.entity.contract.Address;

import java.util.List;

public interface AddressRepository {
    Address findByExternalId(String externalId);

    List<Address> findAllOrderCodePostalDesc(int pageNb, int nbPerPage);

    Address create(Address address);

    Address merge(Address address, String addressStr, String city, String region,
                  int postalCode, String country);

    void delete(Address address);
}
