package oc.adventure.address.ms.entity.contract;

import java.util.Set;

public interface TypeAddress {
    void addAdress(Address address);

    void removeAddress(Address address);

    Long getId();

    String getName();

    Set<Address> getAddressSet();

    String getExternalId();
}
