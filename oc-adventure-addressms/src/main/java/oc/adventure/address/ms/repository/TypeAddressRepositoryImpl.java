package oc.adventure.address.ms.repository;

import oc.adventure.address.ms.entity.TypeAddressImpl;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.address.ms.repository.contract.TypeAddresseRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;


@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class TypeAddressRepositoryImpl implements TypeAddresseRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public TypeAddress find(int id){
        return this.entityManager.find(TypeAddress.class, id);
    }

    @Override
    public TypeAddress findByExternalId(String externalId){
        TypedQuery<TypeAddress> typeAddressTypedQuery = this.entityManager.createNamedQuery(TypeAddressImpl.QN.FIND_BY_EXTERNAL_ID, TypeAddress.class)
                .setParameter(DBUtils.EXTERNAL_ID_DB, externalId);
        return JPAUtils.getSingleResult(typeAddressTypedQuery);
    }

    @Override
    public TypeAddress findByName(String name){
        TypedQuery<TypeAddress> typeAddressTypedQuery = this.entityManager.createNamedQuery(TypeAddressImpl.QN.FIND_BY_NAME, TypeAddress.class)
                .setParameter(DBUtils.NAME, name);
        return JPAUtils.getSingleResult(typeAddressTypedQuery);
    }

    @Override
    public List<TypeAddress> findAll(int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(TypeAddressImpl.QN.FIND_ALL_ORDER_NAME_DESC, TypeAddress.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();

    }

    @Override
    public List<TypeAddress> findAllByNameLikeOrderNameDesc(String nameLike, int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(TypeAddressImpl.QN.FIND_ALL_BY_NAME_LIKE_ORDER_NAME_DESC, TypeAddress.class)
                .setParameter("name_like", "%"+nameLike+"%")
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public TypeAddress create(TypeAddress typeAddress){
            TypeAddress typeAddressFound = this.findByName(typeAddress.getName());
            if(typeAddressFound != null)
                return null;

        ((TypeAddressImpl) typeAddress).setExternalId(JPAUtils.GetBase62(8));
        this.entityManager.persist(typeAddress);
        return typeAddress;
    }

    @Override
    public TypeAddress merge(TypeAddress typeAddress, String name){
        if(!this.entityManager.contains(typeAddress))
            this.entityManager.merge(typeAddress);

        TypeAddress typeAddressFound = this.findByName(name);
        if( typeAddressFound != null)
            return null;
        ((TypeAddressImpl) typeAddress).setName(name);
        return typeAddress;
    }

    @Override
    public void delete(TypeAddress typeAddress){
        this.entityManager.remove(typeAddress);
    }

}
