package oc.adventure.address.ms.service;

import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.address.address.AddressCreateInDto;
import oc.adventure.shared.components.dto.address.address.AddressUserOutDto;
import oc.adventure.shared.components.dto.address.address.AddressUserTypeInDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;
import oc.adventure.shared.components.dto.address.user.UserInDto;
import oc.adventure.shared.components.dto.address.user.UserOutDto;
import oc.adventure.address.ms.entity.AddressImpl;
import oc.adventure.address.ms.entity.TypeAddressImpl;
import oc.adventure.address.ms.entity.UserImpl;
import oc.adventure.address.ms.entity.contract.Address;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.address.ms.entity.contract.User;
import oc.adventure.address.ms.repository.contract.AddressRepository;
import oc.adventure.address.ms.repository.contract.TypeAddresseRepository;
import oc.adventure.address.ms.repository.contract.UserRepository;
import oc.adventure.address.ms.service.contract.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository<User> userUserRepository;

    @Autowired
    private TypeAddresseRepository typeAddresseRepository;

    @Autowired
    public JMapper<AddressImpl, AddressCreateInDto> createInDtoToAddressMapper;

    @Autowired
    public JMapper<UserOutDto, UserImpl> userToOutDtoMapper;

    @Autowired
    public JMapper<AddressUserOutDto, AddressImpl> addressToUserOutDtoMapper;

    @Autowired
    public JMapper<UserImpl, UserInDto> inDtoToUserMapper;

    @Autowired
    public JMapper<TypeAddressImpl, TypeAddressOutDto> outDtoToTypeAddressMapper;

    @Autowired
    public JMapper<TypeAddressOutDto, TypeAddressImpl> typeAddressToDtoOutMapper;

    @Override
    public AddressUserOutDto findByExternalId(String externalId){
        Address address = this.addressRepository.findByExternalId(externalId);
        return this.addressUserOutDtoBody(address, address.getUser(), address.getTypeAddress());
    }

    @Override
    public List<AddressUserOutDto> findAllOrderCodePostalDesc(int pageNb, int nbPerPage){
        return this.addressRepository.findAllOrderCodePostalDesc(pageNb, nbPerPage).stream().map(x -> addressToUserOutDtoMapper.getDestination((AddressImpl) x)).collect(Collectors.toList());
    }

    @Override
    public AddressUserOutDto save(AddressCreateInDto addressInDto){
        Address address = this.createInDtoToAddressMapper.getDestination(addressInDto);
        User user = this.userUserRepository.findByExtId(addressInDto.getUserExternalId());
        TypeAddress typeAddress = this.typeAddresseRepository.findByExternalId(addressInDto.getTypeAddressExternalId());

        // TODO : TO BE CONTINUED Mouahahahahahahah
        address = this.addressRepository.create(address);
        if(address != null && user != null && typeAddress != null){
            user.addAddress(address);
            typeAddress.addAdress(address);
        } else {
            return null;
        }

        return this.addressUserOutDtoBody(address, user, typeAddress);
    }

    @Override
    public AddressUserOutDto merge(AddressCreateInDto addressInDto, String externalId) {
        Address address = this.addressRepository.findByExternalId(externalId);
        User user = this.userUserRepository.findByExtId(addressInDto.getUserExternalId());
        TypeAddress typeAddress = this.typeAddresseRepository.findByExternalId(addressInDto.getTypeAddressExternalId());

        if( address == null || user == null ||typeAddress == null)
            return null;

        if(!user.equals(address.getUser()))
            return null;

        if(!typeAddress.equals(address.getTypeAddress()))
            typeAddress.addAdress(address);

        return this.addressUserOutDtoBody(this.addressRepository.merge(address, addressInDto.getAddress(), addressInDto.getCity(), addressInDto.getRegion(), addressInDto.getPostalCode(), addressInDto.getCountry()), user, typeAddress);
    }

    @Override
    public boolean delete(String externalId){
        this.addressRepository.delete(this.addressRepository.findByExternalId(externalId));
        return this.addressRepository.findByExternalId(externalId) == null;
    }

    @Override
    public List<AddressUserOutDto> findAllByUserAndType(AddressUserTypeInDto addressUserTypeInDto) {
        User user = this.userUserRepository.findByExtId(addressUserTypeInDto.getUserExternalId());
        TypeAddress typeAddress = this.typeAddresseRepository.findByExternalId(addressUserTypeInDto.getTypeAddressExternalId());
        return null;
    }

    private AddressUserOutDto addressUserOutDtoBody(Address address, User user, TypeAddress typeAddress){
        AddressUserOutDto addressOutDto = this.addressToUserOutDtoMapper.getDestination((AddressImpl) address);
        addressOutDto.setUserOutDto(this.userToOutDtoMapper.getDestination((UserImpl) user));
        addressOutDto.setTypeAddress(this.typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddress));
        return addressOutDto;
    }

}
