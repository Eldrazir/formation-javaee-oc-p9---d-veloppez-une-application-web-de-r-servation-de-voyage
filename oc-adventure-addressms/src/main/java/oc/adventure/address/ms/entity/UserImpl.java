package oc.adventure.address.ms.entity;

import oc.adventure.address.ms.entity.contract.Address;
import oc.adventure.address.ms.entity.contract.User;
import oc.adventure.shared.components.entity.UserParentImpl;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class UserImpl extends UserParentImpl implements User {

    @OneToMany(mappedBy = "user", targetEntity = AddressImpl.class,  fetch = FetchType.LAZY, orphanRemoval = true)
    Set<Address> addressList = new HashSet<>();

    public UserImpl() {
    }

    @Override
    public void addAddress(Address address){
        ((AddressImpl) address).setUser(this);
        this.addressList.add(address);
    }

    @Override
    public void removeAddress(Address address){
        this.addressList.remove(address);
    }


    @Override
    public Set<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(Set<Address> addressList) {
        this.addressList = addressList;
    }
}
