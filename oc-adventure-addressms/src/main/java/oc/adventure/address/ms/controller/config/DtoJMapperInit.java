package oc.adventure.address.ms.controller.config;

import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.address.address.AddressCreateInDto;
import oc.adventure.shared.components.dto.address.address.AddressOutDto;
import oc.adventure.shared.components.dto.address.address.AddressUserOutDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressInDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;
import oc.adventure.shared.components.dto.address.user.UserInDto;
import oc.adventure.shared.components.dto.address.user.UserOutDto;
import oc.adventure.address.ms.entity.AddressImpl;
import oc.adventure.address.ms.entity.TypeAddressImpl;
import oc.adventure.address.ms.entity.UserImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DtoJMapperInit {

    @Bean
    public JMapper<TypeAddressImpl, TypeAddressOutDto> outDtoToTypeAddressMapper() {
        return new JMapper<>(TypeAddressImpl.class, TypeAddressOutDto.class);
    }

    @Bean
    public JMapper<TypeAddressOutDto, TypeAddressImpl> typeAddressToDtoOutMapper() {
        return new JMapper<>(TypeAddressOutDto.class,TypeAddressImpl.class);
    }

    @Bean
    public JMapper<TypeAddressImpl, TypeAddressInDto> inDtoToTypeAddressMapper() {
        return new JMapper<>(TypeAddressImpl.class, TypeAddressInDto.class);
    }

    @Bean
    public JMapper<TypeAddressInDto, TypeAddressImpl> typeAddressToDtoInMapper() {
        return new JMapper<>(TypeAddressInDto.class,TypeAddressImpl.class);
    }

    @Bean
    public JMapper<UserImpl, UserInDto> inDtoToUserMapper(){
        return new JMapper<>(UserImpl.class, UserInDto.class);
    }

    @Bean
    public JMapper<UserInDto, UserImpl> userToInDtoMapper(){
        return new JMapper<>(UserInDto.class, UserImpl.class);
    }

    @Bean
    public JMapper<UserImpl, UserOutDto> outDtoToUserMapper(){
        return new JMapper<>(UserImpl.class, UserOutDto.class);
    }

    @Bean
    public JMapper<UserOutDto, UserImpl> userToOutDtoMapper(){
        return new JMapper<>(UserOutDto.class, UserImpl.class);
    }

    @Bean
    public JMapper<AddressImpl, AddressCreateInDto> createInDtoToAddressMapper(){
        return new JMapper<>(AddressImpl.class, AddressCreateInDto.class);
    }

    @Bean
    public JMapper<AddressUserOutDto, AddressImpl> addressToUserOutDtoMapper(){
        return new JMapper<>(AddressUserOutDto.class, AddressImpl.class);
    }

    @Bean
    public JMapper<AddressOutDto, AddressImpl> addressToOutDtoMapper(){
        return new JMapper<>(AddressOutDto.class, AddressImpl.class);
    }

    @Bean
    public JMapper<AddressImpl, AddressOutDto> outDtoToAdressMapper(){
        return new JMapper<>(AddressImpl.class, AddressOutDto.class);
    }

}
