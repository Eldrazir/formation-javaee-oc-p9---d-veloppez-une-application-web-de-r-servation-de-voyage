package oc.adventure.address.ms.service;

import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressInDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;
import oc.adventure.address.ms.entity.TypeAddressImpl;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.address.ms.repository.contract.TypeAddresseRepository;
import oc.adventure.address.ms.service.contract.TypeAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class TypeAddressServiceImpl implements TypeAddressService {

    @Autowired
    private TypeAddresseRepository typeAddresseRepository;

    @Autowired
    public JMapper<TypeAddressOutDto, TypeAddressImpl> typeAddressToDtoOutMapper;

    @Autowired
    public JMapper<TypeAddressImpl, TypeAddressOutDto> outDtoToTypeAddressMapper;

    @Autowired
    public JMapper<TypeAddressImpl, TypeAddressInDto> inDtoToTypeAddressMapper;

    @Override
    public TypeAddressOutDto findByExternalId(String externalId){
        TypeAddress typeAddress = this.typeAddresseRepository.findByExternalId(externalId);
        return typeAddress == null ? null : this.typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddress);
    }

    @Override
    public TypeAddressOutDto findByName(String name){
        TypeAddress typeAddress = this.typeAddresseRepository.findByName(name);
        return typeAddress == null ? null : this.typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddress);
    }

    @Override
    public List<TypeAddressOutDto> findAll(int pageNb, int nbPerPage){
        return this.typeAddresseRepository.findAll(pageNb, nbPerPage).stream().map(x -> typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) x)).collect(Collectors.toList());
    }


    @Override
    public List<TypeAddressOutDto> findAllByNameLike(String nameLike, int pageNb, int nbPerPage){
        return this.typeAddresseRepository.findAllByNameLikeOrderNameDesc(nameLike, pageNb, nbPerPage).stream().map(x -> typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) x)).collect(Collectors.toList());
    }

    @Override
    public TypeAddressOutDto create(TypeAddressInDto typeAddressInDto){
        TypeAddress typeAddress = this.typeAddresseRepository.create(inDtoToTypeAddressMapper.getDestination(typeAddressInDto));
        return typeAddress == null ? null : this.typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddress);
    }

    @Override
    public TypeAddressOutDto merge(TypeAddressOutDto typeAddressOutDto){
        TypeAddress typeAddress = this.typeAddresseRepository.merge(outDtoToTypeAddressMapper.getDestination(typeAddressOutDto), typeAddressOutDto.getName());
        return typeAddress == null ? null : this.typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddress);
    }

    @Override
    public boolean delete(String externalId){
        TypeAddress typeAddress = this.typeAddresseRepository.findByExternalId(externalId);
        this.typeAddresseRepository.delete(typeAddress);
        return this.typeAddresseRepository.findByExternalId(externalId) == null;
    }

}
