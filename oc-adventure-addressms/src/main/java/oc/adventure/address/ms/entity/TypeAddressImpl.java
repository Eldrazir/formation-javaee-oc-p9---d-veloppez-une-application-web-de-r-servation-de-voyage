package oc.adventure.address.ms.entity;


import oc.adventure.address.ms.entity.contract.Address;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "type_address")
@NamedQueries({
        @NamedQuery(
                name = TypeAddressImpl.QN.FIND_BY_EXTERNAL_ID,
                query =TypeAddressImpl.QN.SELECT_FROM +
                        "WHERE ta.externalId = :external_id"
        ),
        @NamedQuery(
                name = TypeAddressImpl.QN.FIND_BY_NAME,
                query =TypeAddressImpl.QN.SELECT_FROM +
                        "WHERE ta.name = :name"
        ),
        @NamedQuery(
                name = TypeAddressImpl.QN.FIND_ALL_ORDER_NAME_DESC,
                query = TypeAddressImpl.QN.SELECT_FROM +
                        TypeAddressImpl.QN.ORDER_NAME_DESC
        ),
        @NamedQuery(
                name = TypeAddressImpl.QN.FIND_ALL_BY_NAME_LIKE_ORDER_NAME_DESC,
                query = TypeAddressImpl.QN.SELECT_FROM +
                        "WHERE ta.name = :name_like "+
                        TypeAddressImpl.QN.ORDER_NAME_DESC
        )
})
public class TypeAddressImpl implements TypeAddress {

    public static class QN {
            public static final String FIND_BY_EXTERNAL_ID = "TypeAddressImpl.findByExternalId";
            public static final String FIND_BY_NAME = "TypeAddressImpl.findByName";
            public static final String FIND_ALL_ORDER_NAME_DESC = "TypeAddressImpl.findAllOrderNameDesc";
            public static final String FIND_ALL_BY_NAME_LIKE_ORDER_NAME_DESC = "TypeAddressImpl.FindAllByNameLikeOrderNameDesc";

            public static final String SELECT_FROM = "SELECT ta FROM TypeAddressImpl ta ";
            public static final String ORDER_NAME_DESC = " ORDER BY ta.name DESC";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBUtils.ID)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, unique = true, length = 8)
    private String externalId;

    @Column(name = DBUtils.NAME, unique = true)
    private String name;

    @OneToMany(mappedBy = "typeAddress", targetEntity = AddressImpl.class, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Address> addressSet = new HashSet<>();

    @Override
    public void addAdress(Address address){
        ((AddressImpl) address).setTypeAddress(this);
        this.addressSet.add(address);
    }

    @Override
    public void removeAddress(Address address){
        this.addressSet.remove(address);
    }

    public TypeAddressImpl() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Set<Address> getAddressSet() {
        return addressSet;
    }

    public void setAddressSet(Set<Address> addressSet) {
        this.addressSet = addressSet;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TypeAddressImpl)) return false;
        TypeAddressImpl that = (TypeAddressImpl) o;
        return Objects.equals(getExternalId(), that.getExternalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalId());
    }
}
