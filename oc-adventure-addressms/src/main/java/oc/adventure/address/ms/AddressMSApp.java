package oc.adventure.address.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages = "oc.adventure.address.ms")
@EnableSwagger2
public class AddressMSApp
{

    public static void main( String[] args )
    {
        SpringApplication.run(AddressMSApp.class, args);
    }
}
