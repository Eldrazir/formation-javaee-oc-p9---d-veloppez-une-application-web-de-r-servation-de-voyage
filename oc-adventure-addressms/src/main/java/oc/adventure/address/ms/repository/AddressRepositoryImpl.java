package oc.adventure.address.ms.repository;

import oc.adventure.address.ms.entity.AddressImpl;
import oc.adventure.address.ms.entity.contract.Address;
import oc.adventure.address.ms.repository.contract.AddressRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AddressRepositoryImpl implements AddressRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Address findByExternalId(String externalId){
        TypedQuery<Address> addressTypedQuery = this.entityManager.createNamedQuery(AddressImpl.QN.FIND_BY_EXTERNAL_ID, Address.class)
                .setParameter(DBUtils.EXTERNAL_ID_DB, externalId);

        return JPAUtils.getSingleResult(addressTypedQuery);

    }

    @Override
    public List<Address> findAllOrderCodePostalDesc(int pageNb, int nbPerPage){
        return this.entityManager.createNamedQuery(AddressImpl.QN.FIND_ALL_ORDER_POSTAL_CODE_DESC, Address.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
    }

    @Override
    public Address create(Address address){
        ((AddressImpl) address).setExternalId(JPAUtils.GetBase62(8));
        this.entityManager.persist(address);
        return address;
    }

    @Override
    public Address merge(Address address, String addressStr, String city, String region,
                         int postalCode, String country){
        if(!this.entityManager.contains(address))
            this.entityManager.merge(address);
        ((AddressImpl) address).setAddress(addressStr);
        ((AddressImpl) address).setCity(city);
        ((AddressImpl) address).setRegion(region);
        ((AddressImpl) address).setPostalCode(postalCode);
        ((AddressImpl) address).setCountry(country);

        return address;
    }

    @Override
    public void delete(Address address){
        this.entityManager.remove(address);
    }

}
