package oc.adventure.address.ms.repository.contract;

import oc.adventure.address.ms.entity.contract.TypeAddress;

import java.util.List;

public interface TypeAddresseRepository {
    TypeAddress find(int id);

    TypeAddress findByExternalId(String externalId);

    TypeAddress findByName(String name);

    abstract List<TypeAddress> findAll(int pageNb, int nbPerPage);

    List<TypeAddress> findAllByNameLikeOrderNameDesc(String nameLike, int pageNb, int nbPerPage);

    TypeAddress create(TypeAddress typeAddress);

    TypeAddress merge(TypeAddress typeAddress, String name);

    void delete(TypeAddress typeAddress);
}
