package oc.adventure.address.ms.service.contract;

import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressInDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;

import java.util.List;

public interface TypeAddressService {
    TypeAddressOutDto findByExternalId(String externalId);

    TypeAddressOutDto findByName(String name);

    List<TypeAddressOutDto> findAll(int pageNb, int nbPerPage);

    List<TypeAddressOutDto> findAllByNameLike(String nameLike, int pageNb, int nbPerPage);

    TypeAddressOutDto create(TypeAddressInDto typeAddressInDto);

    TypeAddressOutDto merge(TypeAddressOutDto typeAddressOutDto);

    boolean delete(String externalId);
}
