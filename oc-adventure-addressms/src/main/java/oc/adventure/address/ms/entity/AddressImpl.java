package oc.adventure.address.ms.entity;


import oc.adventure.address.ms.entity.contract.Address;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.address.ms.entity.contract.User;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "address")
@NamedQueries({
        @NamedQuery(
                name = AddressImpl.QN.FIND_ALL_ORDER_POSTAL_CODE_DESC,
                query = AddressImpl.QN.SELECT_FROM_ADDRESS +
                        AddressImpl.QN.ORDER_POST_CODE_DESC
        ),
        @NamedQuery(
                name = AddressImpl.QN.FIND_BY_EXTERNAL_ID,
                query = AddressImpl.QN.SELECT_FROM_ADDRESS +
                        "WHERE a.externalId = :external_id"
        )

})
public class AddressImpl implements Address {

    public static class QN {
        public static final String FIND_BY_EXTERNAL_ID = "AddressImpl.findByExternalId";
        public static final String FIND_ALL_ORDER_POSTAL_CODE_DESC = "AddressImpl.findAllOrderByPostalCodeDesc";

        public static final String ORDER_POST_CODE_DESC = "ORDER BY a.postalCode DESC";
        public static final String SELECT_FROM_ADDRESS = "SELECT a FROM AddressImpl a ";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DBUtils.ID)
    private Long id;

    @Column(name = DBUtils.EXTERNAL_ID_DB, unique = true, length = 8)
    private String externalId;

    @Column(name = DBUtils.ADDRESS_DB)
    private String address;

    @Column(name = DBUtils.CITY_DB)
    private String city;

    @Column(name = DBUtils.REGION_BD)
    private String region;

    @Column(name = DBUtils.POSTAL_CODE_DB)
    private Integer postalCode;

    @Column(name = DBUtils.COUNTRY_DB)
    private String country;

    @ManyToOne(targetEntity = UserImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = DBUtils.USER_ID)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = TypeAddressImpl.class)
    @JoinColumn(name = DBUtils.TYPE_ADDRESS_ID)
    private TypeAddress typeAddress;

    public AddressImpl() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public TypeAddress getTypeAddress() {
        return typeAddress;
    }

    public void setTypeAddress(TypeAddress typeAddress) {
        this.typeAddress = typeAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressImpl)) return false;
        AddressImpl address = (AddressImpl) o;
        return Objects.equals(getExternalId(), address.getExternalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExternalId());
    }
}
