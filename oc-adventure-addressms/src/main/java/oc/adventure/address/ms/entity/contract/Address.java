package oc.adventure.address.ms.entity.contract;

public interface Address {
    Long getId();

    String getExternalId();

    String getAddress();

    String getCity();

    String getRegion();

    Integer getPostalCode();

    String getCountry();

    User getUser();

    TypeAddress getTypeAddress();
}
