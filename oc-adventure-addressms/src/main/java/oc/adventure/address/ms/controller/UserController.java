package oc.adventure.address.ms.controller;

import oc.adventure.shared.components.dto.address.user.UserInDto;
import oc.adventure.shared.components.dto.address.user.UserOutDto;
import oc.adventure.address.ms.service.contract.TypeAddressService;
import oc.adventure.address.ms.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ms/address/user/")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TypeAddressService typeAddressService;

    @GetMapping(value    = "/",
            produces = "application/json")
    @ResponseBody
    public List<UserOutDto> getAll(@RequestParam int pageNb, @RequestParam int nbPerPage){
        return this.userService.findAll(pageNb, nbPerPage);
    }

    @GetMapping(value    = "/{externalId}",
            produces = "application/json")
    @ResponseBody
    public UserOutDto getByExternalId(@PathVariable("externalId") String externalId){
        return this.userService.findByExternalId(externalId);
    }

    @GetMapping(value    = "/{externalId}/typeaddress/{typeAddressName}",
            produces = "application/json")
    @ResponseBody
    public UserOutDto getByExternalIdAndTypeAdress(@PathVariable("externalId") String externalId, @PathVariable("typeAddressName") String typeAddress){
        return this.userService.findByExternalIdAndTypeAddress(externalId, typeAddress);
    }

    @PostMapping(value    = "register/",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserInDto register (@RequestBody UserInDto userRegisterDto){
        return this.userService.create(userRegisterDto);
    }

    @PutMapping(value    = "update/",
            produces = "application/json",
            consumes = "application/json")
    @ResponseBody
    public UserInDto update(@RequestBody UserInDto userInDto){
        return this.userService.update(userInDto);
    }

    @DeleteMapping(value    = "delete/{externalId}",
            produces = "application/json")
    @ResponseBody
    public boolean delete(@PathVariable("externalId") String externalId){
        return this.userService.delete(externalId);
    }

}
