package oc.adventure.address.ms.entity.contract;

import oc.adventure.shared.components.entity.contract.UserParent;

import java.util.Set;

public interface User extends UserParent {
    void addAddress(Address address);

    void removeAddress(Address address);

    Set<Address> getAddressList();
}
