package oc.adventure.address.ms.service.contract;

import oc.adventure.shared.components.dto.address.user.UserInDto;
import oc.adventure.shared.components.dto.address.user.UserOutDto;

import java.util.List;

public interface UserService {
    UserOutDto findByExternalId(String externalId);

    List<UserOutDto> findAll(int pageNb, int nbPerPage);

    UserInDto create(UserInDto userInDto);

    UserInDto update(UserInDto userInDto);

    boolean delete(String externalId);

    UserOutDto findByExternalIdAndTypeAddress(String externalId, String typeAddressExId);
}
