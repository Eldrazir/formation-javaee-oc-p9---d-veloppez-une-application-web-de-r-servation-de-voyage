package oc.adventure.address.ms.controller;

import oc.adventure.shared.components.dto.address.address.AddressCreateInDto;
import oc.adventure.shared.components.dto.address.address.AddressUserOutDto;
import oc.adventure.shared.components.dto.address.address.AddressUserTypeInDto;
import oc.adventure.address.ms.service.contract.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/ms/address/")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class AddressController {

    @Autowired
    private AddressService addressService;

    @GetMapping (path = "/{externalId}",
            produces = "application/json") //Z2tYCuYT
    @ResponseBody
    public AddressUserOutDto getByExternalId(@PathVariable(name = "externalId") String externalId){
        return this.addressService.findByExternalId(externalId);
    }

    @GetMapping (path = "/",
    produces = "application/json")
    @ResponseBody
    public List<AddressUserOutDto> getAll(@RequestParam(name = "pageNb") int pageNb, @RequestParam(name = "nbPerPage") int nbPerPage){
        return this.addressService.findAllOrderCodePostalDesc(pageNb, nbPerPage);
    }

    public List<AddressUserOutDto> getAllByUserAndType(@RequestBody AddressUserTypeInDto addressUserTypeInDto, @RequestParam(name = "pageNb") int pageNb, @RequestParam(name = "nbPerPage") int nbPerPage){
        return this.addressService.findAllByUserAndType(addressUserTypeInDto);
    }

    @PostMapping(path = "/register/",
    consumes = "application/json",
    produces = "application/json")
    @ResponseBody
    public AddressUserOutDto register(@RequestBody AddressCreateInDto addressCreateInDto){
        return this.addressService.save(addressCreateInDto);
    }

    @PutMapping (path= "/update/{externalId}",
    consumes = "application/json",
    produces = "application/json")
    @ResponseBody
    public AddressUserOutDto update(@RequestBody AddressCreateInDto addressCreateInDto, @PathVariable(name = "externalId") String externalId){
        return this.addressService.merge(addressCreateInDto, externalId);
    }


}
