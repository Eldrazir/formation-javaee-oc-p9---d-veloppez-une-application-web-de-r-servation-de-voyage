package oc.adventure.address.ms.service.contract;

import oc.adventure.shared.components.dto.address.address.AddressCreateInDto;
import oc.adventure.shared.components.dto.address.address.AddressUserOutDto;
import oc.adventure.shared.components.dto.address.address.AddressUserTypeInDto;

import java.util.List;

public interface AddressService {
    AddressUserOutDto findByExternalId(String externalId);

    List<AddressUserOutDto> findAllOrderCodePostalDesc(int pageNb, int nbPerPage);

    abstract AddressUserOutDto save(AddressCreateInDto addressInDto);

    AddressUserOutDto merge(AddressCreateInDto addressInDto, String externalId);

    boolean delete(String externalId);

    List<AddressUserOutDto> findAllByUserAndType(AddressUserTypeInDto addressUserTypeInDto);
}
