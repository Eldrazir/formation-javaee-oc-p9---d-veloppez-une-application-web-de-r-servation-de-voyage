package oc.adventure.address.ms.service;

import com.googlecode.jmapper.JMapper;
import oc.adventure.shared.components.dto.address.address.AddressOutDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;
import oc.adventure.shared.components.dto.address.user.UserInDto;
import oc.adventure.shared.components.dto.address.user.UserOutDto;
import oc.adventure.address.ms.entity.AddressImpl;
import oc.adventure.address.ms.entity.TypeAddressImpl;
import oc.adventure.address.ms.entity.UserImpl;
import oc.adventure.address.ms.entity.contract.Address;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.address.ms.entity.contract.User;
import oc.adventure.address.ms.repository.contract.TypeAddresseRepository;
import oc.adventure.address.ms.repository.contract.UserRepository;
import oc.adventure.address.ms.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class UserServiceImpl implements UserService {

    @Autowired
    public JMapper<UserImpl, UserInDto> inDtoToUserMapper;

    @Autowired
    public JMapper<UserInDto, UserImpl> userToInDtoMapper;

    @Autowired
    public JMapper<UserImpl, UserOutDto> outDtoToUserMapper;

    @Autowired
    public JMapper<UserOutDto, UserImpl> userToOutDtoMapper;

    @Autowired
    public JMapper<AddressOutDto, AddressImpl> addressToOutDtoMapper;

    @Autowired
    public JMapper<TypeAddressOutDto, TypeAddressImpl>  typeAddressToDtoOutMapper;

    @Autowired
    private UserRepository<User> userRepository;

    @Autowired
    private TypeAddresseRepository typeAddresseRepository;

    @Override
    public UserOutDto findByExternalId(String externalId){
        User user = this.userRepository.findByExtId(externalId);
        if(user == null)
            return null;
        return getUserWithAllInfos(user);
    }

    @Override
    public List<UserOutDto> findAll(int pageNb, int nbPerPage){
        return this.userRepository.findAllOrderLastNameAsc(pageNb, nbPerPage).stream().map( x -> this.userToOutDtoMapper.getDestination((UserImpl) x) ).collect(Collectors.toList());
    }

    @Override
    public UserInDto create(UserInDto userInDto){
        return this.userToInDtoMapper.getDestination((UserImpl) this.userRepository.create(this.inDtoToUserMapper.getDestination(userInDto)));
    }

    @Override
    public UserInDto update(UserInDto userInDto){
        User user = this.userRepository.findByExtId(userInDto.getExternalId());
        if (user == null)
            return null;
        return this.userToInDtoMapper.getDestination((UserImpl) this.userRepository.merge(user, userInDto.getFirstName(), userInDto.getLastName()));
    }

    @Override
    public boolean delete(String externalId){
        User user = this.userRepository.findByExtId(externalId);
        if(user == null)
            return false;
        this.userRepository.delete(user);
        return this.userRepository.findByExtId(externalId) == null;
    }

    @Override
    public UserOutDto findByExternalIdAndTypeAddress(String externalId, String AddressTypeName) {
        User user = this.userRepository.findByExtId(externalId);
        if(user == null)
            return null;
        TypeAddress typeAddress = this.typeAddresseRepository.findByName(AddressTypeName);

        return getUserWithAllInfos(user, typeAddress);
    }

    private UserOutDto getUserWithAllInfos(User user, TypeAddress typeAddress) {
        Set<Address> addressList = user.getAddressList();
        UserOutDto userOutDto = this.userToOutDtoMapper.getDestination((UserImpl) user);
        addressList.forEach(a -> {
            TypeAddress typeAddressBis = a.getTypeAddress();
            if(typeAddressBis.equals(typeAddress)){
                AddressOutDto addressOutDto = this.addressToOutDtoMapper.getDestination((AddressImpl) a);
                addressOutDto.setTypeAddress(typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddressBis));
                userOutDto.getAddressList().add(addressOutDto);
            }
        });
        return userOutDto;
    }


    private UserOutDto getUserWithAllInfos(User user){
        Set<Address> addressList = user.getAddressList();
        UserOutDto userOutDto = this.userToOutDtoMapper.getDestination((UserImpl) user);
        addressList.forEach(a -> {
            TypeAddress typeAddress = a.getTypeAddress();
            AddressOutDto addressOutDto = this.addressToOutDtoMapper.getDestination((AddressImpl) a);
            addressOutDto.setTypeAddress(typeAddressToDtoOutMapper.getDestination((TypeAddressImpl) typeAddress));
            userOutDto.getAddressList().add(addressOutDto);
        });
        return userOutDto;
    }

}
