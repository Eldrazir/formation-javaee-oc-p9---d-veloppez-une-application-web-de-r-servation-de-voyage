package oc.adventure.address.ms.controller.config;


import oc.adventure.address.ms.entity.TypeAddressImpl;
import oc.adventure.address.ms.entity.contract.TypeAddress;
import oc.adventure.address.ms.repository.contract.TypeAddresseRepository;
import oc.adventure.utils.JPAUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Autowired
    private TypeAddresseRepository typeAddresseRepository;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        createTypeAddressIfNotFound("DOMICILE");
        createTypeAddressIfNotFound("FACTURATION");
        alreadySetup = true;
    }


    private void createTypeAddressIfNotFound(String name) {

        TypeAddress typeAddress = typeAddresseRepository.findByName(name);
        if (typeAddress == null) {
            typeAddress = new TypeAddressImpl();
            ((TypeAddressImpl)typeAddress).setExternalId(JPAUtils.GetBase62(8));
            ((TypeAddressImpl)typeAddress).setName(name);
            typeAddresseRepository.create(typeAddress);
        }
    }

}
