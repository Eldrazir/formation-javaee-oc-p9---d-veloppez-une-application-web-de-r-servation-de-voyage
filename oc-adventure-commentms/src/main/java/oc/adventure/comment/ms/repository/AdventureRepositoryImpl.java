package oc.adventure.comment.ms.repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import oc.adventure.comment.ms.entity.AdventureImpl;
import oc.adventure.comment.ms.entity.contract.Adventure;
import oc.adventure.comment.ms.repository.contract.AdventureRepository;
import oc.adventure.shared.components.repository.AdventureRepositoryParentImpl;
import oc.adventure.utils.JPAUtils;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AdventureRepositoryImpl extends AdventureRepositoryParentImpl<Adventure> implements AdventureRepository<Adventure>{

	

	
	@Override
	public Adventure findByExtId(String extId) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_EXT_ID, Adventure.class).setParameter("ext_id", extId);
		return JPAUtils.getSingleResult(AdventureTypedQuery);

	}
	@Override
	public Adventure findByNameAdventure(String nameAdventure) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_NAME, Adventure.class)
				.setParameter("name_adventure", nameAdventure);

		return JPAUtils.getSingleResult(AdventureTypedQuery);

	}

	@Override
	public Adventure findByAdventureRating(int adventureRating) {
		TypedQuery<Adventure> AdventureTypedQuery = entityManager
				.createNamedQuery(AdventureImpl.QN.FIND_BY_ADVENTURE_RATING, Adventure.class)
				.setParameter("adventure_rating", adventureRating);

		return JPAUtils.getSingleResult(AdventureTypedQuery);
	}

	@Override
	public void delete(Adventure adventure) {
	this.entityManager.remove(adventure);
		
	}

	@Override
	public Class<Adventure> getEntityTClass() {
		return Adventure.class;
	}

}
