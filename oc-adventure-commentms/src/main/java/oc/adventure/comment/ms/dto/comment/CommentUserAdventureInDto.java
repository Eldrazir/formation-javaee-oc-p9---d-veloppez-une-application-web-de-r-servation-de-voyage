package oc.adventure.comment.ms.dto.comment;

public class CommentUserAdventureInDto extends CommentDto {

	 private String userExternalId;


	    private String adventureExternalId;


	    public CommentUserAdventureInDto() {
	    }


		public String getUserExternalId() {
			return userExternalId;
		}


		public void setUserExternalId(String userExternalId) {
			this.userExternalId = userExternalId;
		}


		public String getAdventureExternalId() {
			return adventureExternalId;
		}


		public void setAdventureExternalId(String adventureExternalId) {
			this.adventureExternalId = adventureExternalId;
		}

	    
	    
}



   
