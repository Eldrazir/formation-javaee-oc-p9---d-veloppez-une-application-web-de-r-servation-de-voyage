package oc.adventure.comment.ms.dto.comment;

import com.googlecode.jmapper.annotations.JMap;

import oc.adventure.comment.ms.dto.adventure.AdventureDto;
import oc.adventure.shared.components.dto.address.typeaddress.TypeAddressOutDto;

public class CommentOutDto extends CommentDto {

	@JMap
	private String externalId;

	private AdventureDto adventure;

	public CommentOutDto() {

	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public AdventureDto getAdventure() {
		return adventure;
	}

	public void setAdventure(AdventureDto adventure) {
		this.adventure = adventure;
	}

}
