package oc.adventure.comment.ms.repository.contract;

import java.util.List;


import oc.adventure.shared.components.entity.contract.AdventureParent;

import oc.adventure.shared.components.repository.AdventureRepositoryParent;


public interface AdventureRepository  <T extends AdventureParent> extends AdventureRepositoryParent<T> {
   

    T findByExtId(String extId);

    T findByNameAdventure(String nameAdventure);

    T findByAdventureRating(int adventureRating);
    
   
    void delete(T adventure);

  
 
  
 
}
