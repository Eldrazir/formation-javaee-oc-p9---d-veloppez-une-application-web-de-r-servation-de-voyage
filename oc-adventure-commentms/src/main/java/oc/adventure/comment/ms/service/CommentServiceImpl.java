package oc.adventure.comment.ms.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.jmapper.JMapper;

import oc.adventure.comment.ms.dto.adventure.AdventureDto;
import oc.adventure.comment.ms.dto.comment.CommentDto;
import oc.adventure.comment.ms.dto.comment.CommentUserAdventureInDto;
import oc.adventure.comment.ms.dto.comment.CommentUserOutDto;
import oc.adventure.comment.ms.dto.user.UserOutDto;
import oc.adventure.comment.ms.entity.AdventureImpl;
import oc.adventure.comment.ms.entity.CommentImpl;
import oc.adventure.comment.ms.entity.UserImpl;
import oc.adventure.comment.ms.entity.contract.Adventure;
import oc.adventure.comment.ms.entity.contract.Comment;
import oc.adventure.comment.ms.entity.contract.User;
import oc.adventure.comment.ms.repository.contract.AdventureRepository;
import oc.adventure.comment.ms.repository.contract.CommentRepository;
import oc.adventure.comment.ms.repository.contract.UserRepository;
import oc.adventure.comment.ms.service.contract.CommentService;
import oc.adventure.shared.components.dto.address.address.AddressCreateInDto;
import oc.adventure.shared.components.dto.address.address.AddressUserOutDto;
import oc.adventure.shared.components.dto.address.address.AddressUserTypeInDto;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private UserRepository<User> userRepository;

	@Autowired
	private AdventureRepository<Adventure>  adventureRepository;

	@Autowired
	private JMapper<CommentDto, CommentImpl> commentToDtoMapper;

	@Autowired
	private JMapper<CommentImpl, CommentDto> commentDtoMapper;
	
	 @Autowired
	    public JMapper<CommentImpl, CommentUserAdventureInDto> createInDtoToCommentMapper;

	 @Autowired
	    public JMapper<CommentUserOutDto, CommentImpl> commentToUserOutDtoMapper;

	@Autowired
	private JMapper<UserOutDto, UserImpl> userToDtoMapper;

	@Autowired
	private JMapper<UserImpl, UserOutDto> userDtoMapper;
	
	@Autowired
	private JMapper<AdventureDto, AdventureImpl> adventureToDtoMapper;

	@Autowired
	private JMapper<AdventureImpl, AdventureDto> adventureDtoMapper;
	

	@Override
	public CommentUserOutDto findByExternalId(String externalId) {
		Comment comment = this.commentRepository.findByExternalId(externalId);
		return commentToUserOutDtoMapper.getDestination((CommentImpl)comment );
	}

	@Override
	public List<CommentUserOutDto> findAll(int pageNb, int nbPerPage) {
		List<Comment> commentList = this.commentRepository.findAll(pageNb, nbPerPage);
		return commentList.stream().map(x -> commentToUserOutDtoMapper.getDestination((CommentImpl) x))
				.collect(Collectors.toList());
	}

	@Override
	public List<CommentUserOutDto> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage) {
		List<Comment> commentList = this.commentRepository.findAllByAdventureId(adventureId, pageNb, nbPerPage);
		return commentList.stream().map(x -> commentAdventureUserFromDto(x, x.getUser(),x.getAdventure()))
				.collect(Collectors.toList());

	}

	public List<CommentUserOutDto> findAllByUserAndAdventure(CommentUserAdventureInDto commentUserAdventureInDto) {
        User user = this.userRepository.findByExtId(commentUserAdventureInDto.getUserExternalId());
        Adventure adventure = this.adventureRepository.findByExtId(commentUserAdventureInDto.getAdventureExternalId());
       
        return null;
    }
	
	@Override
	public List<CommentUserOutDto> findAllByUserId(String userId, int pageNb, int nbPerPage) {
		List<Comment> commentList = this.commentRepository.findAllByUserId(userId, pageNb, nbPerPage);
		return commentList.stream().map(x -> commentToUserOutDtoMapper.getDestination((CommentImpl) x))
				.collect(Collectors.toList());

	}

	@Override
	public CommentUserOutDto save(CommentUserAdventureInDto commentDto) {

		Comment comment = this.createInDtoToCommentMapper.getDestination(commentDto);
		User user = this.userRepository.findByExtId(commentDto.getUserExternalId());
		Adventure adventure = this.adventureRepository.findByExtId(commentDto.getAdventureExternalId());
		
		comment = this.commentRepository.create(comment);
		if(comment != null && user != null && adventure != null){
            user.addComment(comment);
            adventure.addComment(comment);
        } else {
            return null;
        }

		return this.commentAdventureUserFromDto(comment, user, adventure);
	}

	@Override
	public boolean delete(String externalId) {
		this.commentRepository.delete(this.commentRepository.findByExternalId(externalId));
		return this.commentRepository.findByExternalId(externalId) == null;
	}

	@Override
	public CommentUserOutDto merge(CommentUserAdventureInDto commentDto, String externalId) {
		
		Comment comment = this.commentRepository.findByExternalId(externalId);
		User user = this.userRepository.findByExtId(externalId);
		Adventure adventure = this.adventureRepository.findByExtId(externalId);
		
		if (comment ==null || user ==null || adventure == null)
			return null;
		
		if(!user.equals(comment.getUser()))
			return null;
		
		if (!adventure.equals(comment.getAdventure()))
			adventure.addComment(comment);
		

		return this.commentAdventureUserFromDto(this.commentRepository.merge(comment, commentDto.getCommentInput(), commentDto.getDateComment()), user, adventure);

	}
	
	
	private CommentUserOutDto commentAdventureUserFromDto(Comment comment, User user, Adventure adventure) {
		CommentUserOutDto commentDto = this.commentToUserOutDtoMapper.getDestination((CommentImpl) comment);
		commentDto.setUserOutDto (this.userToDtoMapper.getDestination((UserImpl) user));
		commentDto.setAdventure(this.adventureToDtoMapper.getDestination((AdventureImpl) adventure));
		return commentDto;
	}

}
