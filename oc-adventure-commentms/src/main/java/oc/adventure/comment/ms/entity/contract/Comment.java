package oc.adventure.comment.ms.entity.contract;

import java.util.Date;

public interface Comment {
	
	Long getId();
	
	String getExternalId();
	
	String commentInput();
	
	User getUser();
	
	Adventure getAdventure();
	
	Date getDateComment();

}
