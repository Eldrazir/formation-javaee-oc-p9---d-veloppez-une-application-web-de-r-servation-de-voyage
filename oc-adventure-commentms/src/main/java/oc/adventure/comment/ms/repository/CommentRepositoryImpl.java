package oc.adventure.comment.ms.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import oc.adventure.comment.ms.entity.CommentImpl;
import oc.adventure.comment.ms.entity.contract.Adventure;
import oc.adventure.comment.ms.entity.contract.Comment;
import oc.adventure.comment.ms.entity.contract.User;
import oc.adventure.comment.ms.repository.contract.CommentRepository;
import oc.adventure.utils.DBUtils;
import oc.adventure.utils.JPAUtils;


@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CommentRepositoryImpl implements CommentRepository{

	@Autowired
    private EntityManager entityManager;

	@Override
	public Comment findByExternalId(String externalId) {
		TypedQuery<Comment> commentTypedQuery = this.entityManager.createNamedQuery(CommentImpl.QN.FIND_BY_EXTERNAL_ID, Comment.class)
                .setParameter(DBUtils.EXTERNAL_ID_DB, externalId);

        return JPAUtils.getSingleResult(commentTypedQuery);
	}

	@Override
	public List<Comment> findAll(int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(CommentImpl.QN.FIND_ALL, Comment.class)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}

	@Override
	public Comment merge(Comment comment, String inputComment, Date dateComment) {

		if(!this.entityManager.contains(comment))
			this.entityManager.merge(comment);
		
		//((CommentImpl) comment).setUser(user);
		((CommentImpl) comment).setCommentInput(inputComment);
		//((CommentImpl) comment).setAdventure(adventure);
		((CommentImpl) comment).setDateComment(dateComment);
		
		return comment;
	}

	@Override
	public void delete(Comment comment) {
		if(!this.entityManager.contains(comment))
			this.entityManager.merge(comment);
		
		this.entityManager.remove(comment);
		
	}

	@Override
	public Comment save(Comment comment) {
		String externalId = JPAUtils.GetBase62(8);
		Comment CommentFoundByExternal = this.findByExternalId(externalId);
        while(CommentFoundByExternal != null){
            externalId = JPAUtils.GetBase62(8);
            CommentFoundByExternal = this.findByExternalId(externalId);
        }
        this.entityManager.persist(comment);
        return comment;
	}

	@Override
	public List<Comment> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(CommentImpl.QN.FIND_ALL_BY_ADVENTURE_ID, Comment.class).setParameter("adventure_id", adventureId)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}

	@Override
	public List<Comment> findAllByUserId(String userId, int pageNb, int nbPerPage) {
		return this.entityManager.createNamedQuery(CommentImpl.QN.FIND_ALL_BY_USER_ID, Comment.class).setParameter("user_id", userId)
                .setFirstResult((pageNb-1) * pageNb)
                .setMaxResults(nbPerPage)
                .getResultList();
	}

	@Override
	public Comment create(Comment comment) {
		((CommentImpl) comment).setExternalId(JPAUtils.GetBase62(8));
        this.entityManager.persist(comment);
        return comment;

	
	}
	
}
