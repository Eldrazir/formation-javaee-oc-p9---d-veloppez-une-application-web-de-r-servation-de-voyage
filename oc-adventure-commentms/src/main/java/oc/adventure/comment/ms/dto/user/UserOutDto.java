package oc.adventure.comment.ms.dto.user;

import java.util.ArrayList;
import java.util.List;

import oc.adventure.comment.ms.dto.comment.CommentDto;

public class UserOutDto extends UserInDto {

	
	 private List<CommentDto> commentList = new ArrayList();
	 
	 
	public UserOutDto() {
		
	}


	public List<CommentDto> getCommentList() {
		return commentList;
	}


	public void setCommentList(List<CommentDto> commentList) {
		this.commentList = commentList;
	}
	
	
}
