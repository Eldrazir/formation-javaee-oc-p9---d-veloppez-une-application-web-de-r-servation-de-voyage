package oc.adventure.comment.ms.repository.contract;

import oc.adventure.comment.ms.entity.contract.User;
import oc.adventure.shared.components.entity.contract.UserParent;
import oc.adventure.shared.components.repository.UserRepositoryParent;

public interface UserRepository <T extends UserParent> extends UserRepositoryParent<T> {

	 User create(User user);

	    User merge(User user, String firstName, String lastName);

	    void delete(User user);

}
