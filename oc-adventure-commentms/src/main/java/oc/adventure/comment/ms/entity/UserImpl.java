package oc.adventure.comment.ms.entity;

import java.util.Set;

import oc.adventure.comment.ms.entity.contract.Comment;
import oc.adventure.comment.ms.entity.contract.User;
import oc.adventure.shared.components.entity.UserParentImpl;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;


import oc.adventure.utils.DBUtils;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(name = DBUtils.USER)

public class UserImpl extends UserParentImpl implements User  {

		 
	public UserImpl() {
    }

	    
	 @OneToMany(mappedBy = "user", targetEntity = CommentImpl.class,  fetch = FetchType.LAZY, orphanRemoval = true)
	    Set<Comment> commentList = new HashSet<>();

	 
	
	@Override
	public void addComment(Comment comment) {
		((CommentImpl) comment).setUser(this);
        this.commentList.add(comment);

		
	}

	@Override
	public void removeComment(Comment comment) {
		 this.commentList.remove(comment);
		
	}

	@Override
	public Set<Comment> getCommentList() {
		return commentList;
	}
	
	public void setCommentList(Set<Comment> commentList) {
        this.commentList = commentList;
    }

}
