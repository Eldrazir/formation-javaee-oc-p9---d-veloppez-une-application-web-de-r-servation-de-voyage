package oc.adventure.comment.ms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.googlecode.jmapper.JMapper;

import oc.adventure.comment.ms.dto.adventure.AdventureDto;
import oc.adventure.comment.ms.dto.comment.CommentCreateDto;
import oc.adventure.comment.ms.dto.comment.CommentDto;
import oc.adventure.comment.ms.dto.comment.CommentOutDto;
import oc.adventure.comment.ms.dto.comment.CommentUserAdventureInDto;
import oc.adventure.comment.ms.dto.comment.CommentUserOutDto;
import oc.adventure.comment.ms.dto.user.UserOutDto;
import oc.adventure.comment.ms.entity.AdventureImpl;
import oc.adventure.comment.ms.entity.CommentImpl;
import oc.adventure.comment.ms.entity.UserImpl;
import oc.adventure.shared.components.dto.address.user.UserInDto;



@Configuration
public class DtoJMapperInit {
	@Bean
	JMapper<CommentDto, CommentImpl> commentDtoToMapper() {
		return new JMapper<>(CommentDto.class, CommentImpl.class);
	}

	@Bean
	JMapper<CommentImpl, CommentDto> commentMapper() {
		return new JMapper<>(CommentImpl.class, CommentDto.class);
	}

	@Bean
	JMapper<CommentCreateDto, CommentImpl> commentCreateDtoToMapper() {
		return new JMapper<>(CommentCreateDto.class, CommentImpl.class);
	}
	
	@Bean
	JMapper<CommentImpl, CommentCreateDto> commentCreateMapper() {
		return new JMapper<>(CommentImpl.class, CommentCreateDto.class);
	}
	
	@Bean
	JMapper<CommentOutDto, CommentImpl> commentOutDtoToMapper() {
		return new JMapper<>(CommentOutDto.class, CommentImpl.class);
	}

	@Bean
	JMapper<CommentImpl, CommentOutDto> commentOutMapper() {
		return new JMapper<>(CommentImpl.class, CommentOutDto.class);
	}
	
	@Bean
	JMapper<CommentUserOutDto, CommentImpl> commentUserOutDtoToMapper() {
		return new JMapper<>(CommentUserOutDto.class, CommentImpl.class);
	}

	@Bean
	JMapper<CommentImpl, CommentUserOutDto> commentUserOutMapper() {
		return new JMapper<>(CommentImpl.class, CommentUserOutDto.class);
	}
	
	@Bean
	JMapper<CommentUserAdventureInDto, CommentImpl> commentUserAdventureInDtoToMapper() {
		return new JMapper<>(CommentUserAdventureInDto.class, CommentImpl.class);
	}
	
	@Bean
	JMapper<CommentImpl, CommentUserAdventureInDto> commentUserAdventureInDtoMapper() {
		return new JMapper<>(CommentImpl.class, CommentUserAdventureInDto.class);
	}
	
	
	@Bean
	public JMapper<UserImpl, UserInDto> inDtoToUserMapper() {
		return new JMapper<>(UserImpl.class, UserInDto.class);
	}

	@Bean
	public JMapper<UserInDto, UserImpl> userToInDtoMapper() {
		return new JMapper<>(UserInDto.class, UserImpl.class);
	}

	@Bean
	public JMapper<UserImpl, UserOutDto> outDtoToUserMapper() {
		return new JMapper<>(UserImpl.class, UserOutDto.class);
	}

	@Bean
	public JMapper<UserOutDto, UserImpl> userToOutDtoMapper() {
		return new JMapper<>(UserOutDto.class, UserImpl.class);
	}

	@Bean
	JMapper<AdventureDto, AdventureImpl> adventureDtoToMapper() {
		return new JMapper<>(AdventureDto.class, AdventureImpl.class);
	}

	@Bean
	JMapper<AdventureImpl, AdventureDto> adventureapper() {
		return new JMapper<>(AdventureImpl.class, AdventureDto.class);
	}
	
	
}
