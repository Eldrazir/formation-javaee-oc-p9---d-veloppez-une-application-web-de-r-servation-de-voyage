package oc.adventure.comment.ms.entity;

import oc.adventure.comment.ms.entity.contract.Adventure;
import oc.adventure.comment.ms.entity.contract.Comment;
import oc.adventure.comment.ms.entity.contract.User;
import oc.adventure.utils.DBUtils;

import javax.persistence.*;

import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = DBUtils.COMMENT)
@NamedQueries({
	@NamedQuery(
            name = CommentImpl.QN.FIND_ALL,
            query = "SELECT c FROM CommentImpl c "
    ),
	 @NamedQuery(
	            name = CommentImpl.QN.FIND_ALL_BY_USER_ID,
	            query = "SELECT c FROM CommentImpl c " +
	                    "WHERE c.user.externalId = :user_id"
	    ),
    @NamedQuery(
            name = CommentImpl.QN.FIND_BY_EXTERNAL_ID,
            query = "SELECT c FROM CommentImpl c " +
                    "WHERE c.externalId = :external_id"
    ),
	 @NamedQuery(
	            name = CommentImpl.QN.FIND_ALL_BY_ADVENTURE_ID,
	            query = "SELECT c FROM CommentImpl c " +
	                    "WHERE c.adventure.externalId = :adventure_id"
	    )
})





public class CommentImpl implements Comment {

	public static class QN {
		public static final String FIND_BY_EXTERNAL_ID = "CommentImpl.findByExternalId";
		public static final String FIND_ALL = "CommentImpl.findAll";
		
		public static final String FIND_ALL_BY_ADVENTURE_ID = "CommentImpl.findAllByAdventureId";
		public static final String FIND_ALL_BY_USER_ID = "CommentImpl.findAllByUserId";
		public static final String SELECT_FROM_COMMENT = "SELECT c FROM CommentImpl a";

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = DBUtils.ID)
	private Long id;

	@Column(name = DBUtils.EXTERNAL_ID_DB, unique = true, length = 8)
	private String externalId;

	@Column(name = DBUtils.COMMENT_INPUT_DB)
	private String commentInput;

	@Column(name = DBUtils.DATE_COMMENT_DB)
	private Date dateComment;
	
	@ManyToOne(targetEntity = UserImpl.class, fetch = FetchType.LAZY)
	@JoinColumn(name = DBUtils.USER_ID)
	private User user;
	
	@ManyToOne(targetEntity = AdventureImpl.class, fetch = FetchType.LAZY)
	@JoinColumn(name = DBUtils.ADVENTURE_ID_DB)
	private Adventure adventure;

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public String getExternalId() {
		return externalId;
	}

	@Override
	public String commentInput() {
		return commentInput;
	}

	public String getCommentInput() {
		return this.commentInput;
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public Adventure getAdventure() {
		return adventure;
	}
	
	public void setAdventure(Adventure adventure) {
		this.adventure = adventure;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setCommentInput(String commentInput) {
		this.commentInput = commentInput;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof CommentImpl)) return false;
	        CommentImpl comment = (CommentImpl) o;
	        return Objects.equals(getExternalId(), comment.getExternalId());
	    }

	 @Override
	    public int hashCode() {
	        return Objects.hash(getExternalId());
	    }   

	@Override
	public Date getDateComment() {
		
		return dateComment;
	}

	public void setDateComment(Date dateComment) {
		this.dateComment = dateComment;
	}
	
	

		

}
