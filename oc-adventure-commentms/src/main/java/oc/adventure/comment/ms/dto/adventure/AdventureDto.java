package oc.adventure.comment.ms.dto.adventure;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import com.googlecode.jmapper.annotations.JMap;

import oc.adventure.utils.DBUtils;

public class AdventureDto implements Serializable {

	@JMap // Pour indiquer qu'il doit automatiquement Mapper cet attribut avec l'attribut
			// de même nom de l'entité User (hibernate)
	private String externalId;

	@JMap
	private String name;

	@JMap
	private int totalVoters;

	@JMap
	private int totalVoting;

	@JMap
	private int adventureRating;

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalVoters() {
		return totalVoters;
	}

	public void setTotalVoters(int totalVoters) {
		this.totalVoters = totalVoters;
	}

	public int getTotalVoting() {
		return totalVoting;
	}

	public void setTotalVoting(int totalVoting) {
		this.totalVoting = totalVoting;
	}

	public int getAdventureRating() {
		return adventureRating;
	}

	public void setAdventureRating(int adventureRating) {
		this.adventureRating = adventureRating;
	}

}
