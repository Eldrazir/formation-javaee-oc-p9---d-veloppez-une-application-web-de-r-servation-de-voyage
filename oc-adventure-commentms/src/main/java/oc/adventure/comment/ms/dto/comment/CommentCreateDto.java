package oc.adventure.comment.ms.dto.comment;

public class CommentCreateDto extends CommentDto {

	 private String adventureExternalId;


	    private String userExternalId;


		public String getAdventureExternalId() {
			return adventureExternalId;
		}


		public void setAdventureExternalId(String adventureExternalId) {
			this.adventureExternalId = adventureExternalId;
		}


		public String getUserExternalId() {
			return userExternalId;
		}


		public void setUserExternalId(String userExternalId) {
			this.userExternalId = userExternalId;
		}

	    
}
