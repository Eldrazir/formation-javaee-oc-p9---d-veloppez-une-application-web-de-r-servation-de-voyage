package oc.adventure.comment.ms.repository.contract;

import java.util.Date;
import java.util.List;

import oc.adventure.comment.ms.entity.contract.Adventure;
import oc.adventure.comment.ms.entity.contract.Comment;
import oc.adventure.comment.ms.entity.contract.User;

public interface CommentRepository {
	Comment findByExternalId(String externalId);

	List<Comment> findAll(int pageNb, int nbPerPage);

	Comment merge(Comment comment, String inputComment, Date dateComment);

	void delete(Comment comment);

	Comment save(Comment comment);

	List<Comment> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage);
	
	List<Comment> findAllByUserId(String userId, int pageNb, int nbPerPage);
	
	Comment create (Comment comment);
}
