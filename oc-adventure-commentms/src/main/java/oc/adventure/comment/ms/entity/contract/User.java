package oc.adventure.comment.ms.entity.contract;

import java.util.Set;

import oc.adventure.shared.components.entity.contract.UserParent;



public interface User extends UserParent {

	void addComment(Comment comment);
	
	void removeComment (Comment comment);
	
	Set<Comment> getCommentList();

}
