package oc.adventure.comment.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages = "oc.adventure.comment.ms")
@EnableSwagger2
public class CommentMSApp {

	public static void main(String[] args) {
		SpringApplication.run(CommentMSApp.class, args);
	}

}
