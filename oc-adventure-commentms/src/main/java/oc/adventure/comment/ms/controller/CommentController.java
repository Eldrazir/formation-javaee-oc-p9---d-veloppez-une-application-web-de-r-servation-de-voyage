package oc.adventure.comment.ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import oc.adventure.comment.ms.dto.comment.CommentUserAdventureInDto;
import oc.adventure.comment.ms.dto.comment.CommentUserOutDto;
import oc.adventure.comment.ms.service.contract.CommentService;

@RestController
@RequestMapping("/api/v1/ms/comment/")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CommentController {

	@Autowired
	private CommentService commentService;

	@GetMapping(path = "/{externalId}", produces = "application/json")
	@ResponseBody
	public CommentUserOutDto getByExternalId(@PathVariable(name = "externalId") String externalId) {
		return this.commentService.findByExternalId(externalId);
	}
	

	@GetMapping(path = "/", produces = "application/json")
	@ResponseBody
	public List<CommentUserOutDto> getAll(@RequestParam int pageNg, @RequestParam int nbPerPage){
		return this.commentService.findAll(pageNg, nbPerPage);
	}
	
	@PutMapping(path = "/update/{externalId}", consumes = "application/json", produces = "application/json")
	@ResponseBody
	public CommentUserOutDto update(@RequestBody CommentUserAdventureInDto commentDto,
			@PathVariable(name = "externalId") String externalId) {
		return this.commentService.merge(commentDto, externalId);
	}
	
	@PostMapping(path = "/register/", consumes = "application/json", produces = "application/json")
	@ResponseBody
	public CommentUserOutDto register(@RequestBody CommentUserAdventureInDto commentDto) {
		return this.commentService.save(commentDto);
	}
	
	@GetMapping(value = "/adventure/{externalId}",
			produces = "application/json")	
	@ResponseBody
	public List<CommentUserOutDto> getAllByAdventureId(@PathVariable("externalId") String adventureId, @RequestParam int pageNb, @RequestParam int nbPerPage){
		return this.commentService.findAllByAdventureId(adventureId, pageNb, nbPerPage);
	}
}
