package oc.adventure.comment.ms.entity.contract;

import java.util.Set;

import oc.adventure.shared.components.entity.contract.AdventureParent;

public interface Adventure extends AdventureParent {

	Set<Comment> getCommentList();
	
	String getName();

	String getExternalId();

	Integer getId();

	int getAdventureRating();
	
void addComment(Comment comment);
	
	void removeComment (Comment comment);
	
}
