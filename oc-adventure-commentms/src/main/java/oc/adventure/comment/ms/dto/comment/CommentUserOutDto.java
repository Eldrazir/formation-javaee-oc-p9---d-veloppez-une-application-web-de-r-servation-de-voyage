package oc.adventure.comment.ms.dto.comment;

import oc.adventure.comment.ms.dto.user.UserOutDto;

public class CommentUserOutDto extends CommentOutDto{


    private UserOutDto userOutDto;

    public CommentUserOutDto() {
    }

    public UserOutDto getUserOutDto() {
        return userOutDto;
    }

    public void setUserOutDto(UserOutDto userOutDto) {
        this.userOutDto = userOutDto;
    }
}
