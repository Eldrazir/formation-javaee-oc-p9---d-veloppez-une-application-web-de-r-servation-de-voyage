package oc.adventure.comment.ms.entity;

import java.util.HashSet;
import java.util.Set;


import oc.adventure.comment.ms.entity.contract.Adventure;
import oc.adventure.comment.ms.entity.contract.Comment;
import oc.adventure.shared.components.entity.AdventureParentImpl;
import oc.adventure.utils.DBUtils;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = DBUtils.ADVENTURE)
@NamedQueries({
	@NamedQuery(
            name = AdventureImpl.QN.FIND_BY_NAME,
            query = AdventureImpl.QN.SELECT_FROM +
                    "WHERE  a.name = :name_adventure"
    ),

    @NamedQuery(
            name = AdventureImpl.QN.FIND_BY_ADVENTURE_RATING,
            query = AdventureImpl.QN.SELECT_FROM +
                    "WHERE a.adventureRating = :adventure_rating"
    ),
    @NamedQuery(
			name = AdventureParentImpl.QN.FIND_BY_EXT_ID, 
			query = AdventureParentImpl.QN.SELECT_FROM
		+ "WHERE a.externalId = :ext_id"
		)
})
public class AdventureImpl implements Adventure {

	public static class QN {
		public static final String FIND_BY_EXT_ID = "AdventureImpl.findByExtID";
		public static final String FIND_BY_NAME = "AdventureImpl.findByName";
		  public static final String FIND_BY_ADVENTURE_RATING = "AdventureImpl.findByAdventureRating";
		public static final String FIND_ALL = "AdventureImpl.findAll";
		public static final String SELECT_FROM = "SELECT a FROM AdventureImpl a ";
	}
	
	
	@Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
	@Column(name = DBUtils.EXTERNAL_ID_DB, nullable = false, unique = true)
    private String externalId;
	
	@Column(name = DBUtils.NAME_ADVENTURE_DB)
    private String name;
	
	@OneToMany(mappedBy = "adventure", targetEntity = CommentImpl.class,  fetch = FetchType.LAZY, orphanRemoval = true)
	private Set<Comment> commentList = new HashSet<>();
	
	
	 @Column(name = DBUtils.TOTAL_VOTERS_DB)
	    private int totalVoters;

	    @Column(name = DBUtils.TOTAL_VOTING_DB)
	    private int totalVoting;
	    
	    @Column(name = DBUtils.ADVENTURE_RATING_DB)
	    private int adventureRating;
	
	
	@Override
	public Set<Comment> getCommentList() {
		
		return commentList;
	}

	@Override
	public String getName() {
		return this.name;
	}
	
	public void setCommentList(Set<Comment> commentList) {
		this.commentList = commentList;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getAdventureRating() {
		
		return this.adventureRating;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setTotalVoters(int totalVoters) {
		this.totalVoters = totalVoters;
	}

	public void setTotalVoting(int totalVoting) {
		this.totalVoting = totalVoting;
	}

	public void setAdventureRating(int adventureRating) {
		this.adventureRating = adventureRating;
	}

	@Override
	public void addComment(Comment comment) {
		((CommentImpl) comment).setAdventure(this);
        this.commentList.add(comment);

		
	}

	@Override
	public void removeComment(Comment comment) {
		 this.commentList.remove(comment);
		
	}

	@Override
	public String getExternalId() {
	
		return this.externalId;
	}

	@Override
	public Integer getId() {
	
		return this.id;
	}

	@Override
	public String getNameAdventure() {
		
		return this.name;
	}

	public int getTotalVoters() {
		return this.totalVoters;
	}

	public int getTotalVoting() {
		return this.totalVoting;
	}


	


	
	
	

}
