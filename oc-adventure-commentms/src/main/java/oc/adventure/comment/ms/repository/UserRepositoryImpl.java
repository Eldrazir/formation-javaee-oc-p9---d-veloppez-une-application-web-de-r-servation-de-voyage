package oc.adventure.comment.ms.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import oc.adventure.comment.ms.entity.UserImpl;
import oc.adventure.comment.ms.entity.contract.User;
import oc.adventure.comment.ms.repository.contract.UserRepository;
import oc.adventure.shared.components.repository.UserRepositoryParentImpl;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class UserRepositoryImpl extends UserRepositoryParentImpl<User> implements UserRepository<User>{

	@Override
	public User create(User user) {
		if (this.findByExtId(user.getExternalId()) != null)
            return null;
        this.entityManager.persist(user);
        return user;

	}

	@Override
	public User merge(User user, String firstName, String lastName) {
		if(!this.entityManager.contains(user))
            this.entityManager.merge(user);

        ((UserImpl) user).setFirstName(firstName);
        ((UserImpl) user).setLastName(lastName);
        return user;
    }

	  @Override
	    public void delete(User user){
	        this.entityManager.remove(user);
	    }

	    @Override
	    public Class<User> getEntityTClass() {
	        return User.class;
	    }


	

}
