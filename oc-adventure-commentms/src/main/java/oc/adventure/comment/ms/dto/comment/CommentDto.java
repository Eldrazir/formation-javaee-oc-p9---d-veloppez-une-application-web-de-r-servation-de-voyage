package oc.adventure.comment.ms.dto.comment;


import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.googlecode.jmapper.annotations.JMap;

import oc.adventure.comment.ms.dto.adventure.AdventureDto;
import oc.adventure.comment.ms.dto.user.UserOutDto;


public class CommentDto implements Serializable {

	@JMap
	private String externalId;

	@JMap
	private String commentInput;
	
	@JMap
	private Date dateComment;

	


	public CommentDto() {

	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getCommentInput() {
		return commentInput;
	}

	public void setCommentInput(String commentInput) {
		this.commentInput = commentInput;
	}

	public Date getDateComment() {
		return dateComment;
	}

	public void setDateComment(Date dateComment) {
		this.dateComment = dateComment;
	}

	
	
}
