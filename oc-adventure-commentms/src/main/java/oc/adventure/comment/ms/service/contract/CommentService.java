package oc.adventure.comment.ms.service.contract;

import java.util.List;

import oc.adventure.comment.ms.dto.comment.CommentDto;
import oc.adventure.comment.ms.dto.comment.CommentUserAdventureInDto;
import oc.adventure.comment.ms.dto.comment.CommentUserOutDto;
import oc.adventure.comment.ms.entity.contract.Comment;

public interface CommentService {
	
	CommentUserOutDto findByExternalId(String externalId);

	List<CommentUserOutDto> findAll(int pageNb, int nbPerPage);
	
	List<CommentUserOutDto> findAllByAdventureId(String adventureId, int pageNb, int nbPerPage);
	
	List<CommentUserOutDto> findAllByUserId(String userId, int pageNb, int nbPerPage);
	
	abstract CommentUserOutDto save(CommentUserAdventureInDto commentDto);
	
	 boolean delete(String externalId);
	 CommentUserOutDto merge(CommentUserAdventureInDto commentDto, String externalId);
}
